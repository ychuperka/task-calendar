<?php

namespace app\models;

use app\models\actions\Created;
use app\models\actions\Modified;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "task".
 *
 * @property string $id
 * @property string $task_group_id
 * @property string $task_type_id
 * @property string $creator_id
 * @property string $user_id
 * @property string $created_at
 * @property string $completed_at
 * @property string $name
 * @property string $actual_to
 * @property string $status
 * @property string $for_all
 *
 * @property ActionHistory[] $actionHistories
 * @property ChatMessage[] $chatMessages
 * @property TaskGroup $taskGroup
 * @property TaskType $taskType
 * @property User $creator
 * @property User $user
 */
class Task extends \yii\db\ActiveRecord
{
    const STATUS_NOT_COMPLETED = 0;
    const STATUS_COMPLETED = 1;
    const STATUS_POSTPONED = 2;
    public $creator_robot; 

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        // If user can`t update task (not just own tasks) then "user_id" is not required
        return [
            [['task_group_id', 'task_type_id', 'name'], 'required'],
            [['task_group_id', 'task_type_id', 'creator_id', 'creator_robot', 'user_id', 'status'], 'integer'],
            [['for_all'], 'boolean'],
            [['actual_to'], 'date', 'format' => 'php:' . Yii::$app->params['actual_to_date_format_php']],
            [['name'], 'string', 'max' => 128],
            [['status'], 'in', 'range' => [self::STATUS_NOT_COMPLETED, self::STATUS_COMPLETED, self::STATUS_POSTPONED]],
            [
                ['name'],
                'filter',
                'filter' => function ($value) {
                    return strip_tags(
                        trim($value)
                    );
                }
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'task_group_id' => Yii::t('app', 'Task Group ID'),
            'task_type_id' => Yii::t('app', 'Task Type ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Name'),
            'actual_to' => Yii::t('app', 'Actual To'),
            'status' => Yii::t('app', 'Status'),
            'for_all' => Yii::t('app', 'For All'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionHistories()
    {
        return $this->hasMany(ActionHistory::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChatMessages()
    {
        return $this->hasMany(ChatMessage::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskGroup()
    {
        return $this->hasOne(TaskGroup::className(), ['id' => 'task_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskType()
    {
        return $this->hasOne(TaskType::className(), ['id' => 'task_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return array
     */
    public function getActionClassesAsListItems()
    {
        return Action::prepareActionClassesAsListItems(
            Action::getActionClassesForATaskGroupAndTaskType($this->task_group_id, $this->task_type_id)
        );
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            /*
             * If task is not for all then set task`s user id as current authorized user id
             * else set task`s user id to null(mean for all)
             *
             * If task is not for all and user id is specified and it is not current user id and current user has no permission
             * to change user id of the task, then set task user id as current user id.
             */
            if ((!$this->for_all && !$this->user_id)
                || (!$this->for_all && $this->user_id !== Yii::$app->user->id && !Yii::$app->user->can('change_task_user'))
            ) {
                $this->user_id = Yii::$app->user->id;
            } else if ($this->for_all && $this->user_id) {
                /*
                 * If is for all but user id is specified set user id to null
                 */
                $this->user_id = null;
            }

            // Prepare actual date
            if ($this->actual_to) {
                $actualDt = \DateTime::createFromFormat(Yii::$app->params['actual_to_date_format_php'], $this->actual_to);
                if ($actualDt == false) {
                    $this->addError('actual_to', Yii::t('app', 'Invalid format'));
                    return false;
                }
                $this->actual_to = $actualDt->format('Y-m-d');
            } else {
                $this->actual_to = date('Y-m-d');
            }

            // Do things if it is a new record
            if ($this->isNewRecord) {

                // Set initial status
                $this->status = Task::STATUS_NOT_COMPLETED;

                // Set creator id
                if (!$this->creator_id) {
                        // Если creator_id не установлен
                    ///$this->creator_id = Yii::$app->user->id;
                    $this->creator_id = Yii::$app->user->id; //создатель задания
                }
                /*
                if($this->creator_id==29) { 
                    $this->creator_id = Yii::$app->user->id;
                    $this->creator_robot=29;
                    }
                    */
                
            }

            // Is status == COMPLETED then set completed_at datetime
            if ($this->status == Task::STATUS_COMPLETED) {
                $this->completed_at = date('Y-m-d H:i:s');
            }

            // Check a task type in a correct task group
            $taskTypeToTaskGroup = TaskTypeToTaskGroup::find()
                ->select(['task_type_id'])
                ->andWhere(
                    [
                        'task_group_id' => $this->task_group_id
                    ]
                )
                ->all();
                //->limit(10);
            $taskTypes = [];
            foreach ($taskTypeToTaskGroup as $item) {
                $taskTypes[] = $item->task_type_id;
            }
            if (!in_array($this->task_type_id, $taskTypes)) {
                $this->addError('task_type_id', Yii::t('app', 'The task type is not in correct task group'));
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws actions\Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            // Execute "Created" action
            if($this->creator_robot){ // $this->creator_robot=29 // if($this->creator_id==33)
                    $this->creator_id = $this->creator_robot; //создатель действия
                    //$this->note = 188; //создатель действия
                }
            $createdAct = new Created([], $this);
            $createdAct->run();
            $createdAct->historyRecord((string)$this);
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $string = Yii::t('app', 'Status') . ': ' . self::numericStatusToDisplayString($this->status) . PHP_EOL
            . Yii::t('app', 'Name') . ': ' . $this->name . PHP_EOL
            . Yii::t('app', 'Task Group ID') . ': ' . $this->taskGroup->description . PHP_EOL
            . Yii::t('app', 'Task Type ID') . ': ' . $this->taskType->description . PHP_EOL
            . Yii::t('app', 'For All') . ': ' . ($this->for_all ? Yii::t('app', 'Yes') : Yii::t('app', 'No')) . PHP_EOL
            . Yii::t('app', 'Actual To') . ': ' . $this->actual_to;
        if (!$this->for_all) {
            $string .= PHP_EOL . Yii::t('app', 'User ID') . ': ' . $this->user->display_name . '(' . $this->user->username . ')';
        }

        return $string;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        // Prepare actual date
        if (isset($this->actual_to) && $this->actual_to !== null) {
            $actualDt = \DateTime::createFromFormat('Y-m-d', $this->actual_to);
            if ($actualDt !== false) {
                $this->actual_to = $actualDt->format(Yii::$app->params['actual_to_date_format_php']);
            }
        }

        // Prepare completed date
        if ($this->completed_at) {
            $this->completed_at
                = Yii::$app->formatter->asDate($this->completed_at, 'php:' . Yii::$app->params['actual_to_date_format_php']);
        }
    }

    /**
     * @param int $value
     * @return string
     */
    public static function numericStatusToString($value)
    {
        switch ($value) {
            case self::STATUS_NOT_COMPLETED:
                return 'not_completed';
            case self::STATUS_COMPLETED:
                return 'completed';
            case self::STATUS_POSTPONED:
                return 'postponed';
            default:
                return 'unknown';
        }
    }

    /**
     * @param int $value
     * @return string
     */
    public static function numericStatusToDisplayString($value)
    {
        switch ($value) {
            case Task::STATUS_NOT_COMPLETED:
                $taskStatusAsString = Yii::t('app', 'Actual');
                break;
            case Task::STATUS_COMPLETED:
                $taskStatusAsString = Yii::t('app', 'Completed');
                break;
            case Task::STATUS_POSTPONED:
                $taskStatusAsString = Yii::t('app', 'Postponed');
                break;
            default:
                $taskStatusAsString = Yii::t('app', 'Unknown');
                break;
        }

        return $taskStatusAsString;
    }
}
