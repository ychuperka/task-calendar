<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

/**
 * Class PaidAndSetAsCompleted
 *
 * 1. Set task status as "Completed"
 *
 * @package app\models\actions
 */
class PaidAndSetAsCompleted extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '7.' . Yii::t('app', 'Paid');
    }

    public static function getIntersectionNames()
    {
        return [
            'finance_full_payment',
        ];
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        $this->setTaskStatus(Task::STATUS_COMPLETED);
    }
}