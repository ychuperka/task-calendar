<?php

namespace app\models\actions;

use Yii;
use app\models\Task;
use app\models\Action;

/**
 * Class PaidAndSetAsCompletedAndCreateADouble
 *
 * 1. Set task as "Completed"
 * 2. Create task double with following params:
 * new_task->name = old_task->name,
 * new_task->group_id = "Work",
 * new_task->type_id = "Author",
 * new_task->actual_to = "Today",
 * new_task->user_id = old_task->user_id,
 *
 *
 * @package app\models\actions
 */
class PaidAppointedTime extends Appointed
{

    public static function getName()
    {
        return '49.' . Yii::t('app', 'PaidAppointedTime');
    }

    public static function getIntersectionNames()
    {
        return [
            'finance_payment',
            'finance_additional_item_payment',
            'finance_distribution_answer_authors',
            'urgent_payment',
        ];
    }

   
}