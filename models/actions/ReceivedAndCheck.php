<?php

namespace app\models\actions;

use app\models\Task;
use app\models\Action;
use app\models\User;
use Yii;

/**
 * Class ReceivedAndCheck
 *
 * 1. Set task status as "Completed"
 * 2. Create new task with following attributes:
 * new_task->name = old_task->name,
 * new_task->task_group = "Check",
 * new_task->task_type = "Two weeks",
 * new_task->actual_to = "+2 Weeks from Today",
 * new_task->user_id = old_task->user_id,
 *
 * 3. Create new task with following attributes:
 * new_task->name = old_task->name,
 * new_task->task_group_id = "Check",
 * new_task->task_type_id = "Month",
 * new_task->actual_to = "+1 Month from Today",
 * new_task->user_id = old_task->user_id,
 *
 * @package app\models\actions
 */
class ReceivedAndCheck extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '18.' . Yii::t('app', 'Received');
    }

    /**
     * @return array
     */
    public static function getIntersectionNames()
    {
        return [
            'work_ready',
            'problem_ready',
            'other_ready',
            'urgent_ready',
        ];
    }

    public function run()
    {
        
        // Find a payer
        $username = Yii::$app->params['payer_user_login'];
        $payer = User::find()
            ->select('id')
            ->where(['username' => $username])
            ->one();
        if ($payer === null) {
            throw new Exception(Yii::t('app', 'Can not find the user "{username}"', ['username' => $username]));
        }
        
        // Set task status as completed
        $task = $this->getTask();
        $task->status = Task::STATUS_COMPLETED;


        // Create first new task (check in 2 weeks)
        $dt = new \DateTime();

        $twoWeekAfterTodayDt = clone $dt;
        $twoWeekAfterTodayDt->add(\DateInterval::createFromDateString('+2 weeks'));
        

        $newTask = parent::createNewTask(
            'check', 'two_weeks',
            [
                'name' => $task->name,
                'actual_to' => $twoWeekAfterTodayDt->format(Yii::$app->params['actual_to_date_format_php']),
                'user_id' => $task->user_id,
                'for_all' => true,                
                'creator_robot'=> 29, 
            ],
            false
        );

        if (!$newTask->save()) {
            throw new Exception(Yii::t('app', 'Can not save task'));
        }

        // Create second new task (check in 1 month)
        $monthAfterTodayDt = clone $dt;
        $monthAfterTodayDt->add(\DateInterval::createFromDateString('+1 months - 3 day'));

        $secondNewTask = parent::createNewTask(
            'check', 'month',
            [
                'name' => $task->name,
                'actual_to' => $monthAfterTodayDt->format(Yii::$app->params['actual_to_date_format_php']),
                'user_id' => $task->user_id,
                'for_all' => true,
                 'creator_robot'=> 29,
            ],
            false
        );
        

        if (!$secondNewTask->save()) {
            $newTask->delete();
            throw new Exception(Yii::t('app', 'Can not save task'));
        }
        
        
        
       //////////
        // Create second new task (check in 1 month)
        $monthAfterTodayDt2 = clone $dt;
        $monthAfterTodayDt2->add(\DateInterval::createFromDateString('+1 months'));

        $secondNewTask2 = parent::createNewTask(
            'finance', 'pay_to_author',
            [
                'name' => $task->name,
                'actual_to' => $monthAfterTodayDt2->format(Yii::$app->params['actual_to_date_format_php']),
                'user_id' => $payer->id,
                'for_all' => false,
                 'creator_robot'=> 29,
            ],
            false
        );
        
        if (!$secondNewTask2->save()) {
            $newTask->delete();
            $secondNewTask->delete();
            throw new Exception(Yii::t('app', 'Can not save task'));
        }
        /////////////
        
        
        if (!$task->save()) {
            $newTask->delete();
            $secondNewTask->delete();
            $secondNewTask2->delete();
            throw new Exception(Yii::t('app', 'Can not save task'));
        }
        
        /*
         // Find a payer
        $username = Yii::$app->params['payer_user_login'];
        $payer = User::find()
            ->select('id')
            ->where(['username' => $username])
            ->one();
        if ($payer === null) {
            throw new Exception(Yii::t('app', 'Can not find the user "{username}"', ['username' => $username]));
        }

        // Set task status as "Completed"
        $task = $this->getTask();
        $task->status = Task::STATUS_COMPLETED;

        // Create new task
        $params = $this->getParams();
        $actualTo = isset($params['optional_date'])
            ? $params['optional_date'] : date(Yii::$app->params['actual_to_date_format_php']);
        $newTask = parent::createNewTask(
            'finance', 'pay_to_author',
            [
                'name' => $task->name,
                'actual_to' => $actualTo,
                'user_id' => $payer->id,
                'for_all' => false,
                'creator_robot'=> 29, 
            ]
        );

        if (!$task->save()) {
            $newTask->delete();
            throw new Exception(Yii::t('app', 'Can not save task'));
        }
       */ 
        
    }
}