<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

/**
 * Class Appointed
 *
 * 1. Set a task as "Completed"
 * 2. Create new task with following attributes:
 * new_task->name = old_task->name,
 * new_task->group_id = "Work",
 * new_task->type_id = "Competence",
 * new_task->actual_to = null,
 * new_task->user_id = old_task->user_id,
 *
 * 3. Create new task with following attributes:
 * new_task->name = old_task->name,
 * new_task->group_id = "Work",
 * new_task->type_id = "Ready",
 * new_task->actual_to = null,
 * new_task->user_id = old_task->user_id,
 *
 * @package app\models\actions
 */
class PartReadyDate extends Action
{
    public static function getName()
    {
        return '39.' . Yii::t('app', 'PartReadyDate');
    }
    
      public function validateParams()
    {
        $params = $this->getParams();
        return isset($params['optional_date']) && strlen($params['optional_date']) > 0;
    }

    public static function getIntersectionNames()
    {
        return [
            'work_warm_time_part',
        ];
    }   

    public function run()
    {        
        

        // Set task status as "Completed"
        $task = $this->getTask();
        $task->status = Task::STATUS_COMPLETED;        
        
    
       
        

        // Create first new task
        $newTask = parent::createNewTask(
            'work', 'finishedpieces',
            [
                'name' => $task->name,
                'actual_to' => $this->getParams()['optional_date'],
                'user_id' => $task->user_id,
                'for_all' => $task->for_all,
                'creator_robot'=> 29, 
            ]
        );

        
        

        // Save original task
        if (!$task->save()) {
            $newTask->delete();           
            throw new Exception(Yii::t('app', 'Can not save task'));
        }
        
               
    }
}