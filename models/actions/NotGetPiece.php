<?php

namespace app\models\actions;

use app\models\Action;
use app\models\ActionHistory;
use app\models\Task;
use app\models\TaskType;
use Yii;

/**
 * Class NoReceivedNTimes
 *
 * If action executes first or second time:
 * 1. Set task status as "Not Completed"
 * 2. Set task actual_to to date passed through params or (if not passed)
 * to the next day (after Today)
 *
 * If action executes second time:
 * 1. Set task status as "Completed"
 * 2. Create new task with following attributes:
 * new_task->name = old_task->name,
 * new_task->task_group_id = "Problem",
 * new_task->task_type_id = old_task->task_type_id,
 * new_task->actual_to = passed through params or empty,
 * new_task->user_id = old_task->user_id,
 *
 * @package app\models\actions
 */
class NotGetPiece extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '47.' . Yii::t('app', 'NotGetPiece');
    }

    /**
     * @return array
     */
    public static function getIntersectionNames()
    {
        return [
            'work_finishedpieces',
        ];
    }

    public function run()
    {
        // Check execution times
        $task = $this->getTask();
        $count = ActionHistory::find()
            ->where(['task_id' => $task->id])
            ->andWhere(['action_class' => (new \ReflectionClass($this))->getShortName()])
            ->count();
        ++$count;

        $secondTimeExecution = ($count % 2) == 0;
        if ($secondTimeExecution) {

            // Set task status as completed
            $task->status = Task::STATUS_COMPLETED;

            // Create new task
            $type = TaskType::findOne($task->task_type_id);
            $params = $this->getParams();
            $newTask = parent::createNewTask(
                'problem', $type->name,
                [
                    'name' => $task->name,
                    'user_id' => $task->user_id,
                    'actual_to' => isset($params['optional_date']) && strlen($params['optional_date']) > 0
                        ? $params['optional_date'] : null,
                    'for_all' => $task->for_all,
                     'creator_robot'=> 29, 
                ]
            );

            if (!$task->save()) {
                $newTask->delete();
                throw new Exception(Yii::t('app', 'Can not save the task'));
            }

        } else {
            // Set task status as not completed
            $task->status = Task::STATUS_NOT_COMPLETED;

            // Set actual_to
            $params = $this->getParams();
            $tomorrowDt = new \DateTime();
            $tomorrowDt->add(\DateInterval::createFromDateString('+1 days'));
            $task->actual_to = isset($params['optional_date']) && strlen($params['optional_date']) > 0
                ? $params['optional_date'] : $tomorrowDt->format(Yii::$app->params['actual_to_date_format_php']);

            if (!$task->save()) {
                throw new Exception(Yii::t('app', 'Can not save task'));
            }
        }

    }
}