<?php

namespace app\models\actions;

use app\models\Action;
use app\models\Task;
use Yii;

class NoCheck extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '26.' . Yii::t('app', 'No Check');
    }

    public static function getIntersectionNames()
    {
        return [
            'check_two_weeks',
            'check_month',
            'check_check',
        ];
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        $this->setTaskStatus(Task::STATUS_COMPLETED);
    }
}