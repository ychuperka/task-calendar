<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

class Exposed extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '12.' . Yii::t('app', 'Exposed');
    }

    public static function getIntersectionNames()
    {
        return [
            'urgent_expose',
            'urgent_expose_fl',
        ];
    }

    /**
     * Run a logic
     *
     * @return mixed
     */
    public function run()
    {
        $this->setTaskStatus(Task::STATUS_COMPLETED);
    }
}