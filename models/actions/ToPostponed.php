<?php
namespace app\models\actions;

use app\models\Action;
use app\models\Task;
use Yii;

/**
 * Class ToPostponed
 * 1. Set task status as "Postponed"
 *
 * @package app\models\actions
 */
class ToPostponed extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '4.' . Yii::t('app', 'To Postponed');
    }

    /**
     * @return array
     */
    public static function getIntersectionNames()
    {
        return [];
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        $this->setTaskStatus(Task::STATUS_POSTPONED);
    }
}