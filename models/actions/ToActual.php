<?php

namespace app\models\actions;

use app\models\Action;
use app\models\Task;
use Yii;

/**
 * Class ToActual
 * 1. Set task status as "Not Completed"
 * 2. Set task actual_to to null
 *
 * @package app\models\actions
 */
class ToActual extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '5.' . Yii::t('app', 'To Actual');
    }

    /**
     * @return array
     */
    public static function getIntersectionNames()
    {
        return [];
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        $task = $this->getTask();
        $task->status = Task::STATUS_NOT_COMPLETED;
        $task->actual_to = null;
        if (!$task->save()) {
            throw new Exception(Yii::t('app', 'Can not save the task'));
        }
    }
}