<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

/**
 * Class ChangeActualTo
 * 1. Set task status as "Not Completed"
 * 2. Set actual_to to the
 *
 * @package app\models\actions
 */
class ChangeActualTo extends Action
{
    public static function getName()
    {
        return '2.' . Yii::t('app', 'Change Actual To');
    }

    public function validateParams()
    {
        $params = $this->getParams();
        return isset($params['optional_date']) && strlen($params['optional_date']) > 0;
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        $task = $this->getTask();

        // Set task status as not completed
        $task->status = Task::STATUS_NOT_COMPLETED;

        // Set task actual_to date
        $task->actual_to = $this->getParams()['optional_date'];

        if (!$task->save()) {
            throw new Exception(Yii::t('app', 'Can not save the task'));
        }
    }

}