<?php

namespace app\models\actions;

use app\models\Action;
use app\models\Task;
use Yii;

/**
 * Class Refused
 *
 * 1. Set task status as "Completed"
 *
 * @package app\models\actions
 */
class Refused extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '3.' . Yii::t('app', 'Refused');
    }

    /**
     * @return array
     */
    public static function getIntersectionNames()
    {
        return [
            'finance_payment',
            'finance_full_payment',
            'finance_additional_item_payment',

            'urgent_payment',
        ];
    }

    public function run()
    {
        $this->setTaskStatus(Task::STATUS_COMPLETED);
    }
}