<?php

namespace app\models\actions;

use Yii;
use app\models\Action;

class NoDial extends Action
{
    public static function getName()
    {
        return '13.' . Yii::t('app', 'No Dial');
    }

    public static function getIntersectionNames()
    {
        return [
            'communication_call',
            'urgent_call',
        ];
    }

    public function run()
    {
        $task = $this->getTask();
        $actualToDt = new \DateTime();
        $actualToDt->add(\DateInterval::createFromDateString('+1 days'));
        $task->actual_to = $actualToDt->format(Yii::$app->params['actual_to_date_format_php']);
        if (!$task->save()) {
            throw new Exception(Yii::t('app', 'Can not save task'));
        }
    }
}