<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

class Adjustments extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '25.' . Yii::t('app', 'Adjustments');
    }

    public static function getIntersectionNames()
    {
        return [
            'check_two_weeks',
            'check_month',
            'check_check',
        ];
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        // Set task status
        $task = $this->getTask();
        $task->status = Task::STATUS_COMPLETED;

        // Create new task
        $params = $this->getParams();
        if (isset($params['optional_date']) && strlen($params['optional_date']) > 0) {
            $actualTo = $params['optional_date'];
        } else {
            $actualTo = date(Yii::$app->params['actual_to_date_format_php']);
        }

        $newTask = parent::createNewTask(
            'work', 'adjustments',
            [
                'name' => $task->name,
                'actual_to' => $actualTo,
                'user_id' => $task->user_id,
                'for_all' => $task->for_all,
                 'creator_robot'=> 29, 
            ],
            false
        );

        if (!$newTask->save()) {
            throw new Exception(Yii::t('app', 'Can not save task:'));
        }

        if (!$task->save()) {
            $newTask->delete();
            throw new Exception(Yii::t('app', 'Can not save task'));
        }
    }
}