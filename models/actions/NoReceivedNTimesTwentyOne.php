<?php

namespace app\models\actions;

use Yii;

class NoReceivedNTimesTwentyOne extends NoReceivedNTimes
{
    public static function getName()
    {
        return '21.' . Yii::t('app', 'No Received');
    }

    public static function getIntersectionNames()
    {
        return [
            'work_adjustments',
            'work_client_answer',
            'work_author_answer',
            'urgent_adjustments',
        ];
    }
}