<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

/**
 * Class ClientAnswer
 *
 * 1. Set task status as "Completed"
 * 2. Create new task with following attributes:
 * new_task->name = old_task->name,
 * new_task->task_group_id = "Work",
 * new_task->task_type_id = "Client Answer",
 * new_task->actual_to = if not passed through params then use current,
 * new_Task->user_id = old_task->user_id,
 *
 * @package app\models\actions
 */
class ClientAnswer extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '23.' . Yii::t('app', 'Client Answer');
    }

    public static function getIntersectionNames()
    {
        return [
            'work_author_answer',
            'problem_author_answer',
            'work_distribution_answer_authors'
        ];
    }

    public function run()
    {
        // Set task status
        $task = $this->getTask();
        $task->status = Task::STATUS_COMPLETED;

        // Create new task
        $params = $this->getParams();
        if (isset($params['optional_date']) && strlen($params['optional_date']) > 0) {
            $actualTo = $params['optional_date'];
        } else {
            $actualTo = date(Yii::$app->params['actual_to_date_format_php']);
        }

        $newTask = parent::createNewTask(
            'work', 'client_answer',
            [
                'name' => $task->name,
                'actual_to' => $actualTo,
                'user_id' => $task->user_id,
                'for_all' => $task->for_all,
                 'creator_robot'=> 29, 
            ],
            false
        );

        if (!$newTask->save()) {
            throw new Exception(Yii::t('app', 'Can not save task'));
        }

        if (!$task->save()) {
            $newTask->delete();
            throw new Exception(Yii::t('app', 'Can not save task'));
        }
    }
}