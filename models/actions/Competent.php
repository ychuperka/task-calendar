<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

class Competent extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '16.' . Yii::t('app', 'Competent');
    }

    public static function getIntersectionNames()
    {
        return [
            'work_competence',
            'urgent_competence',
        ];
    }

    /**
     * Run a logic
     *
     * @return mixed
     */
    public function run()
    {
        $this->setTaskStatus(Task::STATUS_COMPLETED);
    }
}