<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

class SentSocial extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '11.' . Yii::t('app', 'Write');
    }

    public static function getIntersectionNames()
    {
        return [
            'communication_vk',
        ];
    }

    /**
     * Run a logic
     *
     * @return mixed
     */
    public function run()
    {
        $this->setTaskStatus(Task::STATUS_COMPLETED);
    }
}