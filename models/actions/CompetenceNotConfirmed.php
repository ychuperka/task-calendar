<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

/**
 * Class NoCompetent
 *
 * 1. Set task status as "Completed"
 * 2. Create new task with following attributes:
 * new_task->name = old_task->name,
 * new_task->task_group_id = "Work",
 * new_task->task_type_id = "Author",
 * new_task->actual_to = "Today",
 * new_task->user_id = old_task->user_id,
 *
 * @package app\models\actions
 */
class CompetenceNotConfirmed extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '41.' . Yii::t('app', 'CompetenceNotConfirmed');
    }

    public static function getIntersectionNames()
    {
        return [
            'work_competence',
        ];
    }

    /**
     * @throws Exception
     */
    public function run()
    {

        // Set task status as "Completed"
        $task = $this->getTask();
        $task->status = Task::STATUS_COMPLETED;
        
        // Дата
        $dt = new \DateTime();
        $dayAfterTodayDt = clone $dt;
        $dayAfterTodayDt->add(\DateInterval::createFromDateString('+1 day'));

        // Create new task
        $newTask = parent::createNewTask(
            'urgent', 'competence',
            [
                'name' => $task->name,
                'actual_to' => $dayAfterTodayDt->format(Yii::$app->params['actual_to_date_format_php']),
                'user_id' => $task->user_id,
                'for_all' => $task->for_all,
                'creator_robot'=> 29, 
            ],
            false
        );

        if (!$newTask->save()) {
            throw new Exception(Yii::t('app', 'Can not save task'));
        }

        if (!$task->save()) {
            $newTask->delete();
            throw new Exception(Yii::t('app', 'Can not save task'));
        }
    }
}