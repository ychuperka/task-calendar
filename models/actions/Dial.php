<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

class Dial extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '9.' . Yii::t('app', 'Dial');
    }

    public static function getIntersectionNames()
    {
        return [
            'communication_call',
            'urgent_call',
        ];
    }

    /**
     * Run a logic
     *
     * @return mixed
     */
    public function run()
    {
        $this->setTaskStatus(Task::STATUS_COMPLETED);
    }
}