<?php

namespace app\models\actions;

use app\models\Action;
use app\models\Task;
use Yii;

class Paid extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '27.' . Yii::t('app', 'Paid');
    }

    public static function getIntersectionNames()
    {
        return [
            'finance_pay_to_author',
        ];
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        $this->setTaskStatus(Task::STATUS_COMPLETED);
    }
}