<?php
namespace app\models\actions;

use app\models\User;
use Yii;
use app\models\Task;
use app\models\Action;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class Other
 *
 * 1. Set task status as "Postponed"
 *
 * @package app\models\actions
 */
class Other extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '6.' . Yii::t('app', 'OtherZaverch');
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        /*
        $user = User::find()
            ->where(['username' => Yii::$app->params['action_other_user_login']])
            ->one();
        if (!$user) {
            throw new Exception('User "' . Yii::$app->params['action_other_user_login'] . '" not found');
        }
        */
        
        $task = $this->getTask();
        $task->status = Task::STATUS_POSTPONED;  
        //$this->setTaskStatus(Task::STATUS_POSTPONED, false);
        /*$this->getTask()->user_id = $user->id;*/

        if (!$this->getTask()->save()) {
            throw new Exception('Can not save the task');
        }
    }
}