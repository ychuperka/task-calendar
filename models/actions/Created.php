<?php

namespace app\models\actions;

use app\models\ActionHistory;
use Yii;
use app\models\Action;

class Created extends Action
{
    /**
     * @return string
     */
    public static function getName()
    {
        return '31.' . Yii::t('app', 'Created');
    }

    public static function getIntersectionNames()
    {
        return [];
    }

    /**
     * Run a logic
     *
     * @return mixed
     */
    public function run()
    {
    }

    /**
     * @param string $note
     * @param bool $save
     * @return ActionHistory
     * @throws Exception
     */
    public function historyRecord($note = null, $save = true)
    {
        $actionHistory = parent::historyRecord($note, false);
        $actionHistory->user_id = $this->getTask()->creator_id;
        if ($save && !$actionHistory->save()) {
            throw new Exception(Yii::t('app', 'Can not save action history'));
        }
        return $actionHistory;
    }
}