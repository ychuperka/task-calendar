<?php

namespace app\models\actions;

use app\models\Action;
use app\models\Task;
use app\models\User;
use app\models\ActionHistory;
use app\models\TaskType;
use Yii;

/*
use app\models\Action;
use app\models\ActionHistory;
use app\models\Task;
use app\models\TaskType;
use Yii;
*/



/**
 * Class Score
 *
 * 1. Find a payer
 * 2. Set task status as "Completed"
 * 3. Create new task with following attributes:
 * new_task->name = old_task->name,
 * new_task->task_group_id = "Finance",
 * new_task->task_type_id = "Pay to Author",
 * new_task->actual_to = (if not passed through params then use Today),
 * new_task->user_id = Payer (single user, username stored in the app params)
 *
 * @package app\models\actions
 */
class SendUserM extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '33.' . Yii::t('app', 'SendUserM');
    }

   

    /**
     * @throws Exception
     */
     public function run()
    {
        // Check execution times
        $task = $this->getTask();
        

        
            // Set task status as not completed
            $task->status = Task::STATUS_NOT_COMPLETED;

            // Set actual_to
            $params = $this->getParams();
            $tomorrowDt = new \DateTime();
            $tomorrowDt->add(\DateInterval::createFromDateString('today'));
            $task->actual_to = isset($params['optional_date']) && strlen($params['optional_date']) > 0
                ? $params['optional_date'] : $tomorrowDt->format(Yii::$app->params['actual_to_date_format_php']);
            
            $task->user_id = 28;
             $task->for_all = false;

            if (!$task->save()) {
                throw new Exception(Yii::t('app', 'Can not save task'));
            }
        

    }
}