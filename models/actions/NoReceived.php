<?php

namespace app\models\actions;

use app\models\Action;
use app\models\Task;
use Yii;


/**
 * Class NoReceived
 *
 * 1. Set task status as "Not Completed"
 * 2. Set "actual_to" to date received through params
 * or (if not passed) to next day (after Today).
 *
 * @package app\models\actions
 */
class NoReceived extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '29.' . Yii::t('app', 'No Received');
    }

    public static function getIntersectionNames()
    {
        return [
            'problem_ready',
            'problem_adjustments',
            'problem_client_answer',
            'problem_author_answer',
        ];
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        // Set task status as "Not Completed"
        $task = $this->getTask();
        $task->status = Task::STATUS_NOT_COMPLETED;

        // Set actual to
        $params = $this->getParams();
        if (isset($params['optional_date']) && strlen($params['optional_date']) > 0) {
            $actualTo = $params['optional_date'];
        } else {
            $dt = new \DateTime();
            $actualTo = $dt->add(\DateInterval::createFromDateString('+1 days'))->format(Yii::$app->params['actual_to_date_format_php']);
        }
        $task->actual_to = $actualTo;

        if (!$task->save()) {
            throw new Exception(Yii::t('app', 'Can not save task'));
        }

    }
}