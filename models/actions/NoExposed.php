<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

class NoExposed extends Action
{
    public static function getName()
    {
        return '14.' . Yii::t('app', 'No Exposed');
    }

    public static function getIntersectionNames()
    {
        return [
            'urgent_expose',
            'urgent_expose_fl',
        ];
    }

    public function run()
    {
        $this->setTaskStatus(Task::STATUS_COMPLETED);
    }
}