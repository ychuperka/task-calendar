<?php
namespace app\models\actions;

use app\models\User;
use Yii;
use app\models\Task;
use app\models\Action;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class Other
 *
 * 1. Set task status as "Postponed"
 *
 * @package app\models\actions
 */
class RemoveDouble extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '43.' . Yii::t('app', 'RemoveDouble');
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        // Set task status as "Completed"
        $task = $this->getTask();
        $task->status = Task::STATUS_COMPLETED;      
        
        

        if (!$this->getTask()->save()) {
            throw new Exception('Can not save the task');
        }
    }
}