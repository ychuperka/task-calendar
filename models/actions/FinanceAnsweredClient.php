<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

/**
 * Class Appointed
 *
 * 1. Set a task as "Completed"
 * 2. Create new task with following attributes:
 * new_task->name = old_task->name,
 * new_task->group_id = "Work",
 * new_task->type_id = "Competence",
 * new_task->actual_to = null,
 * new_task->user_id = old_task->user_id,
 *
 * 3. Create new task with following attributes:
 * new_task->name = old_task->name,
 * new_task->group_id = "Work",
 * new_task->type_id = "Ready",
 * new_task->actual_to = null,
 * new_task->user_id = old_task->user_id,
 *
 * @package app\models\actions
 */
class FinanceAnsweredClient extends Action
{
    public static function getName()
    {
        return '44.' . Yii::t('app', 'FinanceAnsweredClient');
    }

    public static function getIntersectionNames()
    {
        return [
            'finance_author_answer',   
            'finance_payment',
            'finance_distribution_answer_authors'         
        ];
    }
  

    public function run()
    {
        
        // Set task status as "Completed"
        $task = $this->getTask();
        $task->status = Task::STATUS_COMPLETED;
                
               
        // Create first new task
        $params = $this->getParams();
        $actualTo = isset($params['optional_date'])
            ? $params['optional_date'] : date(Yii::$app->params['actual_to_date_format_php']);
            
        $newTask = parent::createNewTask(
            'finance', 'client_answer',
            [
                'name' => $task->name,  
                'actual_to' => $actualTo,                 
                'user_id' => $task->user_id,
                'for_all' => $task->for_all,
                'creator_robot'=> 29, 
            ]
        );

               
        // Save original task
        if (!$task->save()) {
            $newTask->delete();
            
            throw new Exception(Yii::t('app', 'Can not save task'));
        }
        
               
    }
}