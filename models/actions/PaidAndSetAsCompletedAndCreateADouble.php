<?php

namespace app\models\actions;

use Yii;
use app\models\Task;
use app\models\Action;

/**
 * Class PaidAndSetAsCompletedAndCreateADouble
 *
 * 1. Set task as "Completed"
 * 2. Create task double with following params:
 * new_task->name = old_task->name,
 * new_task->group_id = "Work",
 * new_task->type_id = "Author",
 * new_task->actual_to = "Today",
 * new_task->user_id = old_task->user_id,
 *
 *
 * @package app\models\actions
 */
class PaidAndSetAsCompletedAndCreateADouble extends Action
{

    public static function getName()
    {
        return '1.' . Yii::t('app', 'Paid');
    }

    public static function getIntersectionNames()
    {
        return [
            'finance_payment',
            'finance_additional_item_payment',
            'finance_distribution_answer_authors',
            'urgent_payment',
        ];
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        $task = $this->getTask();

        // Set task as "Completed"
        $task->status = Task::STATUS_COMPLETED;

        // Create task double
        $newTask = parent::createNewTask(
            'work', 'author',
            [
                'name' => $task->name,
                'user_id' => $task->user_id,
                'for_all' => $task->for_all,
                'creator_robot'=> 29, 
               /* 'creator_id'=> 29,*/
            ],
            false
        );

        // Save
        if (!$newTask->save()) {
            throw new Exception(Yii::t('app', 'Can not save task'));
        }

        if (!$task->save()) {
            $newTask->delete();
            throw new Exception(Yii::t('app', 'Can not save task'));
        }
    }
}