<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

class Assigned extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '31.' . Yii::t('app', 'Assigned');
    }

    public static function getIntersectionNames()
    {
        return [
            'exhibitions_warm_time',
        ];
    }

    /**
     * Run a logic
     *
     * @return mixed
     */
    public function run()
    {
        $this->setTaskStatus(Task::STATUS_COMPLETED);
    }
}