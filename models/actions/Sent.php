<?php

namespace app\models\actions;

use Yii;
use app\models\Task;
use app\models\Action;

class Sent extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '10.' . Yii::t('app', 'Sent');
    }

    public static function getIntersectionNames()
    {
        return [
            'communication_email',
        ];
    }

    /**
     * Run a logic
     *
     * @return mixed
     */
    public function run()
    {
        $this->setTaskStatus(Task::STATUS_COMPLETED);
    }
}