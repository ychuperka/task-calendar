<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

class Complete extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '30.' . Yii::t('app', 'Complete');
    }

    public static function getIntersectionNames()
    {
        return [
            'problem_ready',
            'problem_adjustments',
            'problem_client_answer',
            'problem_author_answer',
        ];
    }

    public function run()
    {
        $this->setTaskStatus(Task::STATUS_COMPLETED);
    }
}