<?php

namespace app\models\actions;

use Yii;
use app\models\Task;
use app\models\Action;

class ToCompleted extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '8.' . Yii::t('app', 'Completed');
    }

    /**
     * @return array
     */
    public static function getIntersectionNames()
    {
        return [];
    }

    public function run()
    {
        $this->setTaskStatus(Task::STATUS_COMPLETED);
    }
}