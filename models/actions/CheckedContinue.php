<?php

namespace app\models\actions;

use Yii;
use app\models\Action;
use app\models\Task;

/**
 * Class Appointed
 *
 * 1. Set a task as "Completed"
 * 2. Create new task with following attributes:
 * new_task->name = old_task->name,
 * new_task->group_id = "Work",
 * new_task->type_id = "Competence",
 * new_task->actual_to = null,
 * new_task->user_id = old_task->user_id,
 *
 * 3. Create new task with following attributes:
 * new_task->name = old_task->name,
 * new_task->group_id = "Work",
 * new_task->type_id = "Ready",
 * new_task->actual_to = null,
 * new_task->user_id = old_task->user_id,
 *
 * @package app\models\actions
 */
class CheckedContinue extends Action
{
    public static function getName()
    {
        return '38.' . Yii::t('app', 'CheckedContinue');
    }

    public static function getIntersectionNames()
    {
        return [
            'work_checked_continue',
        ];
    }   

    public function run()
    {        
        

        // Set task status as "Completed"
        $task = $this->getTask();
        $task->status = Task::STATUS_COMPLETED;        
        
        // Дата
        $dt = new \DateTime();
        $dayAfterTodayDt = clone $dt;
        $dayAfterTodayDt->add(\DateInterval::createFromDateString('+1 day'));
       
        

        // Create first new task
        $newTask = parent::createNewTask(
            'work', 'warm_time_part',
            [
                'name' => $task->name,
                'actual_to' => $dayAfterTodayDt->format(Yii::$app->params['actual_to_date_format_php']),
                'user_id' => $task->user_id,
                'for_all' => $task->for_all,
                'creator_robot'=> 29, 
            ]
        );

        
        

        // Save original task
        if (!$task->save()) {
            $newTask->delete();           
            throw new Exception(Yii::t('app', 'Can not save task'));
        }
        
               
    }
}