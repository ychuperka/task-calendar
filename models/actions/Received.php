<?php

namespace app\models\actions;

use app\models\Action;
use app\models\Task;
use Yii;

/**
 * Class Received
 *
 * 1. Set task status as "Completed"
 *
 * @package app\models\actions
 */
class Received extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '20.' . Yii::t('app', 'Received');
    }

    /**
     * @return array
     */
    public static function getIntersectionNames()
    {
        return [
            'work_adjustments',
            'work_client_answer',
            'work_author_answer',
            'work_finishedpieces',
            

            'problem_adjustments',
            'problem_client_answer',
            'problem_author_answer',
            'problem_finishedpieces',
            
            'urgent_adjustments',
            
        ];
    }

    public function run()
    {
        $this->setTaskStatus(Task::STATUS_COMPLETED);
    }
}