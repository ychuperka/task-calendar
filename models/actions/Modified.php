<?php

namespace app\models\actions;

use app\models\Action;
use Yii;

class Modified extends Action
{
    /**
     * @return string
     */
    public static function getName()
    {
        return '32.' . Yii::t('app', 'Modified');
    }

    public static function getIntersectionNames()
    {
        return [];
    }

    /**
     * Run a logic
     *
     * @return mixed
     */
    public function run()
    {
    }
}