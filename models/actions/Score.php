<?php

namespace app\models\actions;

use app\models\Action;
use app\models\Task;
use app\models\User;
use Yii;

/**
 * Class Score
 *
 * 1. Find a payer
 * 2. Set task status as "Completed"
 * 3. Create new task with following attributes:
 * new_task->name = old_task->name,
 * new_task->task_group_id = "Finance",
 * new_task->task_type_id = "Pay to Author",
 * new_task->actual_to = (if not passed through params then use Today),
 * new_task->user_id = Payer (single user, username stored in the app params)
 *
 * @package app\models\actions
 */
class Score extends Action
{

    /**
     * @return string
     */
    public static function getName()
    {
        return '24.' . Yii::t('app', 'Score');
    }

    public static function getIntersectionNames()
    {
        return [
            'check_two_weeks',
            'check_month',
            'check_check',
        ];
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        // Find a payer
        $username = Yii::$app->params['payer_user_login'];
        $payer = User::find()
            ->select('id')
            ->where(['username' => $username])
            ->one();
        if ($payer === null) {
            throw new Exception(Yii::t('app', 'Can not find the user "{username}"', ['username' => $username]));
        }

        // Set task status as "Completed"
        $task = $this->getTask();
        $task->status = Task::STATUS_COMPLETED;

        // Create new task
        $params = $this->getParams();
        $actualTo = isset($params['optional_date'])
            ? $params['optional_date'] : date(Yii::$app->params['actual_to_date_format_php']);
        $newTask = parent::createNewTask(
            'finance', 'pay_to_author',
            [
                'name' => $task->name,
                'actual_to' => $actualTo,
                'user_id' => $payer->id,
                'for_all' => false,
                'creator_robot'=> 29, 
            ]
        );

        if (!$task->save()) {
            $newTask->delete();
            throw new Exception(Yii::t('app', 'Can not save task'));
        }
    }
}