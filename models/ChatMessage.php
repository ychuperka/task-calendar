<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;

/**
 * This is the model class for table "chat_message".
 *
 * @property string $id
 * @property string $user_id
 * @property string $task_id
 * @property string $created_at
 * @property string $message
 *
 * @property User $user
 * @property Task $task
 */
class ChatMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'message'], 'required'],
            [['user_id', 'task_id'], 'integer'],
            [['message'], 'string'],
            [
                ['message'],
                'filter',
                'filter' => function ($value) {
                    return strip_tags(
                        trim($value)
                    );
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'task_id' => Yii::t('app', 'Task ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'message' => Yii::t('app', 'Message'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            // Check task id exists
            if ($this->task_id && !Task::find()->where(['id' => $this->task_id])->exists()) {
                $this->addError('task_id', Yii::t('app', 'The task is not found'));
                return false;
            }

            if ($this->isNewRecord) {
                $this->user_id = Yii::$app->user->id;
            }

            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public static function getMaxCreatedAt()
    {
        return (new Query())
            ->select('MAX(created_at) AS max_created_at')
            ->from('chat_message')
            ->one()['max_created_at'];
    }

    /**
     * @param int $value
     * @param int|null $task_id
     * @return mixed
     */
    public static function getCreatedAtByIndexFromEnd($value, $task_id = null)
    {
        $count = ChatMessage::find()
            ->where(['task_id' => $task_id])
            ->count();
        return (new Query())
            ->select('created_at')
            ->from('chat_message')
            ->where(['task_id' => $task_id])
            ->orderBy('id')
            ->offset($count - $value)
            ->limit(1)
            ->one()['created_at'];
    }
}
