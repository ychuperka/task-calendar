<?php

namespace app\models;

class ChatMessageSearch extends ChatMessage
{
    /**
     * @param $createdAfter
     * @param $taskId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function search($createdAfter, $taskId = null)
    {
        $query = ChatMessage::find();

        $query->where(['>=', 'created_at', $createdAfter])
            ->andWhere(['task_id' => $taskId])
            ->orderBy('id asc');

        return $query->all();
    }

    /**
     * @param int $beforeMessageId
     * @param int $limit
     * @param int|null $taskId
     * @return array
     */
    public static function searchPrevious($beforeMessageId, $limit, $taskId = null)
    {
        $messages = ChatMessage::find()
            ->where(['<', 'id', $beforeMessageId])
            ->andFilterWhere(['task_id' => $taskId])
            ->orderBy('id desc')
            ->limit($limit)
            ->all();

        return array_reverse($messages);
    }
}