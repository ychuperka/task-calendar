<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "action_history".
 *
 * @property string $id
 * @property string $task_id
 * @property string $user_id
 * @property string $action_class
 * @property string $created_at
 * @property string $optional_datetime
 * @property string $note
 *
 * @property Task $task
 */
class ActionHistory extends TaskSlave
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'action_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'user_id', 'action_class'], 'required'],
            [['task_id', 'user_id'], 'integer'],
            [['optional_datetime'], 'date', 'format' => 'php:' . Yii::$app->params['actual_to_date_format_php']],
            [['note'], 'string'],
            [['action_class'], 'string', 'max' => 64],
            [
                ['note', 'action_class'], 'filter',
                'filter' => function ($value) {
                    return strip_tags(
                        trim($value)
                    );
                }
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'task_id' => Yii::t('app', 'Task ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'action_class' => Yii::t('app', 'Action Class'),
            'created_at' => Yii::t('app', 'Created At'),
            'optional_datetime' => Yii::t('app', 'Optional Datetime'),
            'note' => Yii::t('app', 'Note'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->optional_datetime !== null) {
            $dt = \DateTime::createFromFormat(
                Yii::$app->params['actual_to_date_format_php'],
                $this->optional_datetime
            );
            $this->optional_datetime = $dt->format('Y-m-d H:i:s');
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        if ($this->optional_datetime !== null) {
            $dt = \DateTime::createFromFormat('Y-m-d H:i:s', $this->optional_datetime);
            $this->optional_datetime = $dt->format(
                Yii::$app->params['actual_to_date_format_php']
            );
        }
    }

    public static function getListForTasks(array $tasksIds) {
        $result = self::find()
            ->where(['in', 'task_id', $tasksIds])
            ->orderBy('task_id desc')
            ->all();

        $prepared = [];
        foreach ($result as $item) {
            $prepared[$item->task_id][] = $item;
        }

        return $prepared;
    }
}
