<?php

namespace app\models;

use app\components\PasswordOwner;
use app\components\RoleOwner;
use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $auth_key
 * @property string $username
 * @property string $password_hash
 * @property string $name
 *
 * @property ChatMessage[] $chatMessages
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

    use ModelOwner;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'display_name'], 'required'],
            [['username'], 'string', 'min' => 4, 'max' => 32],
            [['username'], 'unique'],
            [['username'], 'match', 'pattern' => '/[A-z0-9_\.]{4,32}/'],
            [['password', 'check'], 'string', 'min' => 8, 'max' => 60],
            [['role'], 'safe'],
            [['display_name'], 'string', 'min' => 4, 'max' => 64],
            [['display_name'], 'filter', 'filter' => function ($value) {
                return strip_tags(
                    trim(
                        $value
                    )
                );
            }]
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => RoleOwner::className(),
            ],
            [
                'class' => PasswordOwner::className(),
                'hash_property' => 'password_hash',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'display_name' => Yii::t('app', 'Display Name'),
            'password' => Yii::t('app', 'Password'),
            'check' => Yii::t('app', 'Check'),
            'role' => Yii::t('app', 'Role'),
        ];
    }

    /**
     * Validates a password
     *
     * @param string $password
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChatMessages()
    {
        return $this->hasMany(ChatMessage::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
            }
            return true;
        }

        return false;
    }

    /**
     * Finds an user by username
     *
     * @param string $username
     * @return null|static
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Realize the method in future, if need
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function getListByTasksIds(array $tasksIds)
    {
        return self::getListBySlavesIds('user', 'user_id', 'task', 'task_id', $tasksIds);
    }

    public static function getListByActionHistoriesIds(array $actionHistoriesIds)
    {
        return self::getListBySlavesIds('user', 'user_id', 'action_history', 'action_history_id', $actionHistoriesIds);
    }
}