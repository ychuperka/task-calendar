<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task_group".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 *
 * @property Task[] $tasks
 */
class TaskGroup extends \yii\db\ActiveRecord
{

    use ModelOwner;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],
            [['name'], 'unique'],
            [['description'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['task_group_id' => 'id']);
    }

    public static function getListByTasksIds(array $tasksIds)
    {
        return self::getListBySlavesIds('task_group', 'task_group_id', 'task', 'task_id', $tasksIds);
    }
}
