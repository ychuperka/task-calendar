<?php

namespace app\models;

use yii\base\InvalidValueException;

class TaskSlave extends \yii\db\ActiveRecord
{
    public static function getListByTasksIds(array $tasksIds)
    {
        // Check tasks ids list
        if (!$tasksIds) {
            throw new InvalidValueException('Tasks ids list is empty');
        }

        $result = static::find()
            ->where(['in', 'task_id', $tasksIds])
            ->orderBy('task_id desc')
            ->all();

        $prepared = [];
        foreach ($result as $item) {
            $prepared[$item->task_id][] = $item;
        }

        return $prepared;
    }
}