<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task_type".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 *
 * @property Task[] $tasks
 */
class TaskType extends \yii\db\ActiveRecord
{

    use ModelOwner;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_type';
    }

    public static function fieldName()
    {
        return 'task_type_id';
    }

    /**
     * @param int $taskGroupId Task group id
     * @param string $name Task type name
     * @return TaskType
     */
    public static function findByTaskGroupIdAndName($taskGroupId, $name)
    {
        $rels = TaskTypeToTaskGroup::find()
            ->select('task_type_id')
            ->where(['task_group_id' => $taskGroupId])
            ->all();

        $taskTypeIds = [];
        foreach ($rels as $item) {
            $taskTypeIds[] = $item->task_type_id;
        }

        return TaskType::find()
            ->where(['in', 'id', $taskTypeIds])
            ->andWhere(['name' => $name])
            ->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],
            [['description'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['task_type_id' => 'id']);
    }

    public static function getListByTasksIds(array $tasksIds)
    {
        return self::getListBySlavesIds('task_type', 'task_type_id', 'task', 'task_id', $tasksIds);
    }
}
