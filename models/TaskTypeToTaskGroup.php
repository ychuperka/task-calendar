<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task_type_to_task_group".
 *
 * @property string $id
 * @property string $task_group_id
 * @property string $task_type_id
 * @property string $intersection_name
 *
 * @property TaskGroup $taskGroup
 * @property TaskType $taskType
 */
class TaskTypeToTaskGroup extends \yii\db\ActiveRecord
{
    private static $intersections;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_type_to_task_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_group_id', 'task_type_id', 'intersection_name'], 'required'],
            [['task_group_id', 'task_type_id'], 'integer'],
            [['intersection_name'], 'string', 'max' => 128],
            [['task_group_id', 'task_type_id'], 'unique', 'targetAttribute' => ['task_group_id', 'task_type_id'], 'message' => 'The combination of Task Group ID and Task Type ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'task_group_id' => Yii::t('app', 'Task Group ID'),
            'task_type_id' => Yii::t('app', 'Task Type ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskGroup()
    {
        return $this->hasOne(TaskGroup::className(), ['id' => 'task_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskType()
    {
        return $this->hasOne(TaskType::className(), ['id' => 'task_type_id']);
    }

    /**
     * @param int $task_group_id
     * @param int $task_type_id
     * @return array|null
     */
    public static function getIntersectionNamesByTaskGroupAndTaskType($task_group_id, $task_type_id)
    {
        $task_group_id = (int)$task_group_id;
        $task_type_id = (int)$task_type_id;
        
        // Load intersections if they are not loaded yet
        if (!self::$intersections) {
            $result = self::find()
                ->select(['task_group_id', 'task_type_id', 'intersection_name'])
                ->all();  
                
            self::$intersections = [];    
            foreach ($result as $row) {
                self::$intersections[(int)$row['task_group_id']][(int)$row['task_type_id']][] = $row->intersection_name;
            }
        }
        
        // Check data exists
        if (!isset(self::$intersections[$task_group_id][$task_type_id])) {
            return null;
        }

        return self::$intersections[$task_group_id][$task_type_id];
    }
}
