<?php

namespace app\models;

use yii\base\InvalidValueException;
use yii\db\Query;

trait ModelOwner
{
    public static function getListBySlavesIds(
        $ownerTableName, $ownerIdColumnNameInASlaveTable, $slaveTableName, $slaveSelectColumnName, array $slaveIds
    )
    {
        if (!$slaveIds) {
            throw new InvalidValueException('Tasks ids list is empty');
        }

        $result = (new Query())
            ->select("`$slaveTableName`.`id` AS `$slaveSelectColumnName`, `$ownerTableName`.*")
            ->from($ownerTableName)
            ->innerJoin($slaveTableName, "`$slaveTableName`.`$ownerIdColumnNameInASlaveTable` = `$ownerTableName`.`id`")
            ->where(['in', "`$slaveTableName`.`id`", $slaveIds])
            ->all();

        $prepared = [];
        foreach ($result as $item) {
            $prepared[$item[$slaveSelectColumnName]] = $item;
        }
        return $prepared;
    }
}