<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\actions\Exception;

class Action extends Model
{

    /**
     * @var Task
     */
    private $task;

    /**
     * @var array
     */
    private $params;

    /**
     * @param array $config
     * @param Task $task
     * @param array $params
     */
    public function __construct(array $config = [], Task $task, array $params = [])
    {
        parent::__construct($config);

        $this->task = $task;
        $this->params = $params;
    }

    /**
     * @return Task
     */
    protected function getTask()
    {
        return $this->task;
    }

    /**
     * @return array|null
     */
    protected function getParams()
    {
        return $this->params;
    }

    /**
     * @return bool
     */
    public function validateParams()
    {
        return true;
    }

    /**
     * @return string
     * @throws Exception
     */
    public static function getName()
    {
        throw new Exception('The method should be override');
    }

    /**
     * @static
     * @return array
     */
    public static function getIntersectionNames()
    {
        return [
            'finance_payment',
            'finance_full_payment',
            'finance_additional_item_payment',
            'finance_pay_to_author',
            'finance_other',
            'finance_author_answer',
            'finance_client_answer',
            'finance_distribution_answer_authors',
            

            'communication_call',
            'communication_email',
            'communication_vk',
            'communication_communic_client',
            'communication_communic_author',

            'urgent_call',
            'urgent_expose',
            'urgent_payment',
            'urgent_adjustments',
            'urgent_other',
            'urgent_competence',
            'urgent_ready',
            'urgent_expose_fl',       

            'work_author',
            'work_competence',
            'work_ready',
            'work_adjustments',
            'work_author_answer',
            'work_client_answer',
            'work_warm_time',
            'work_checked_continue',
            'work_finishedpieces',
            'work_warm_time_part',
            'work_warm_time_adjustments',
            'work_distribution_answer_authors',
            
            

            'check_two_weeks',
            'check_month',
            'check_check',

            'problem_ready',
            'problem_adjustments',
            'problem_client_answer',
            'problem_author_answer',
            'problem_finishedpieces',
            'problem_other',

        ];
    }

    /**
     * Run a logic
     *
     * @return mixed
     * @throws Exception
     */
    public function run()
    {
        throw new Exception(Yii::t('app', 'The method should be override'));
    }

    /**
     * @param string $note
     * @param bool $save
     * @return ActionHistory
     * @throws Exception
     */
    public function historyRecord($note = null, $save = true)
    {
        $params = $this->getParams();
        $actionHistory = new ActionHistory();
        $actionHistory->action_class = (new \ReflectionClass($this))->getShortName();
        $actionHistory->task_id = $this->getTask()->id;
        $actionHistory->user_id = Yii::$app->user->id;
        $actionHistory->optional_datetime = isset($params['optional_date']) && strlen($params['optional_date']) > 0
            ? $params['optional_date']
            : null;
        $actionHistory->note = $note;

        if ($save && !$actionHistory->save()) {
            throw new Exception(Yii::t('app', 'Can not save action history'));
        }

        return $actionHistory;
    }

    /**
     * @param int $status
     * @param bool $save
     * @throws Exception
     */
    protected function setTaskStatus($status, $save = true)
    {
        $task = $this->getTask();
        $task->status = $status;
        if ($save && !$task->save()) {
            $errors = $task->getErrors();
            $messages = PHP_EOL;
            foreach ($errors as $item) {
                $messages .= implode(PHP_EOL, $item);
            }
            throw new Exception(Yii::t('app', 'Can not save the task') . ': ' . $messages);
        }
    }

    /**
     * Creates new task e.
     *
     * @static
     * @param $taskGroupName
     * @param $taskTypeName
     * @param array $attributes
     * @param bool $save
     * @return Task
     * @throws Exception
     */
    protected static function createNewTask($taskGroupName, $taskTypeName, array $attributes, $save = true)
    {

        $group = TaskGroup::find()
            ->where(['name' => $taskGroupName])
            ->one();
        if ($group === null) {
            throw new Exception(Yii::t('app', 'Group not found'));
        }

        $type = TaskType::findByTaskGroupIdAndName($group->id, $taskTypeName);
        if ($type === null) {
            throw new Exception(Yii::t('app', 'Type not found'));
        }

        $task = new Task();
        $task->task_group_id = $group->id;
        $task->task_type_id = $type->id;

        foreach ($attributes as $name => $value) {
            $task->$name = $value;
        }

        if ($save && !$task->save()) {
            throw new Exception(Yii::t('app', 'Can not save task'));
        }

        return $task;
    }

    /**
     * Find all classes
     *
     * @return array
     * @throws Exception
     */
    public static function findAllClasses()
    {
        $directoryPath = __DIR__ . DIRECTORY_SEPARATOR . 'actions';
        if (!is_dir($directoryPath)) {
            throw new Exception(Yii::t('app', 'Directory not found: {directory}', ['directory' => $directoryPath]));
        }

        $iter = new \DirectoryIterator($directoryPath);
        $actionClasses = [];
        foreach ($iter as $item) {
            if (!$item->isFile() || $item->getExtension() !== 'php') {
                continue;
            }

            $actionClasses[] = '\app\models\actions\\' . $item->getBasename('.php');
        }

        return $actionClasses;
    }

    /**
     * @param string $className
     * @return mixed
     * @throws Exception
     */
    public static function convertClassNameToActionName($className)
    {
        $fullClassName = '\app\models\actions\\' . $className;
        if (!class_exists($fullClassName)) {
            throw new Exception(Yii::t('app', 'Action class not found "{class}"', ['class' => $className]));
        }

        if (!method_exists($fullClassName, 'getName')) {
            throw new Exception(Yii::t('app', 'Action method not found "{method}"', ['method' => 'getName']));
        }

        return call_user_func("$fullClassName::getName");
    }

    /**
     * @param int $task_group_id
     * @param int $task_type_id
     * @return array
     * @throws \app\models\actions\Exception
     */
    public static function getActionClassesForATaskGroupAndTaskType($task_group_id, $task_type_id)
    {
        $intersectionNames = TaskTypeToTaskGroup::getIntersectionNamesByTaskGroupAndTaskType($task_group_id, $task_type_id);
        $actionClasses = self::findAllClasses();
        $actionClassesFiltered = [];
        foreach ($actionClasses as $item) {

            if (!method_exists($item, 'getIntersectionNames')) {
                // TODO: Log record
                continue;
            }

            $insnNames = call_user_func("$item::getIntersectionNames");
            foreach ($insnNames as $name) {
                if (in_array($name, $intersectionNames)) {
                    $actionClassesFiltered[] = $item;
                    continue 2;
                }
            }
        }

        return $actionClassesFiltered;
    }

    /**
     * @param array $action_classes
     * @return array
     */
    public static function prepareActionClassesAsListItems(array $action_classes)
    {
        $list = [];
        foreach ($action_classes as $item) {
            if (!method_exists($item, 'getName')) {
                // TODO: Log record
                continue;
            }

            $key = array_slice(
                explode('\\', $item), -1, 1
            )[0];
            $list[$key] = call_user_func("$item::getName");
        }

        return $list;
    }
}