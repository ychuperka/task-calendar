<?php

namespace app\controllers;

use app\models\Action;
use app\models\ActionHistory;
use app\models\actions\Created;
use app\models\actions\Exception as ActionExcpetion;
use app\models\actions\Modified;
use app\models\actions\ToActual;
use app\models\actions\ToCompleted;
use app\models\actions\ToPostponed;
use app\models\ChatMessage;
use app\models\Order;
use app\models\TaskTypeToTaskGroup;
use Yii;
use app\models\Task;
use app\models\TaskGroup;
use app\models\TaskType;
use app\models\User;
use yii\base\ErrorException;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'view', 'create', 'update',
                    'delete', 'get-task-types',
                    'create-double', 'do-action',
                    'set-status',

                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'view' => ['get', 'post'],
                    'create' => ['get', 'post'],
                    'update' => ['post'],
                    'delete' => ['post'],
                    'set-status' => ['post'],
                    'do-action' => ['post'],
                    'get-task-types' => ['get'],
                ],
            ],
        ];
    }

    /**
     * Displays a single Task model.
     * @param string $id
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $user = Yii::$app->getUser();
        if (!$user->can('view_task')
            && !$user->can('view_task', ['model' => $model])
        ) {
            throw new ForbiddenHttpException(Yii::t('app', 'You can not view the task'));
        }

        $viewData = $this->prepareViewData($model);
        return $this->renderAjax('view', $viewData);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param string $actual_to
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws BadRequestHttpException
     */
    public function actionCreate($actual_to = null)
    {
        if (!Yii::$app->getUser()->can('create_task')) {
            throw new ForbiddenHttpException(Yii::t('app', 'You can not create a task'));
        }

        if ($actual_to !== null && !preg_match('/\d{4}\-\d{2}\-\d{2}/', $actual_to)) {
            throw new BadRequestHttpException(Yii::t('app', 'Invalid "{param}" format', ['param' => 'actual_to']));
        }

        $model = new Task();
        $isDataLoaded = $model->load(Yii::$app->request->post());
        $saved = $isDataLoaded && $model->save();
        if (!$saved) {
            $model->actual_to = null;
        }

        if (!$isDataLoaded) {
            $viewData = $this->prepareViewData($model, $saved);
            return $this->renderAjax(
                $saved ? 'view' : 'create',
                array_merge($viewData, ['actual_to' => $actual_to])
            );
        } else {
            return Json::encode(
                [
                    'task_id' => $model->id,
                ]
            );
        }
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->getUser()->can('update_task', ['model' => $model])) {
            throw new ForbiddenHttpException(Yii::t('app', 'You can not update the task'));
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $actModified = new Modified([], $model);
                $actModified->run();
                $actModified->historyRecord((string)$model);
            }
        }

        $viewData = $this->prepareViewData($model, true);
        return $this->renderAjax('view', $viewData);
    }

    /**
     * @return string
     * @throws BadRequestHttpException
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionCreateDouble()
    {
        if (($id = Yii::$app->request->post('task_id', null)) === null) {
            throw new BadRequestHttpException(Yii::t('app', 'Required param "{name}" is missing', ['name' => 'task_id']));
        }

        $model = $this->findModel($id);
        if (!Yii::$app->user->can('create_task_double', ['model' => $model])) {
            throw new ForbiddenHttpException(Yii::t('app', 'You can not create the task double'));
        }

        $double = new Task();
        $double->actual_to = $model->actual_to;
        $double->user_id = Yii::$app->user->id;
        $double->task_type_id = $model->task_type_id;
        $double->task_group_id = $model->task_group_id;
        $double->name = $model->name;
        $double->status = $model->status;
        $double->for_all = $model->for_all;

        $double->save();

        $viewData = $this->prepareViewData($double, true);
        return $this->renderAjax('view', $viewData);
    }

    /**
     * @return string
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionSetStatus()
    {
        if (($id = Yii::$app->request->post('task_id', null)) === null) {
            throw new BadRequestHttpException(Yii::t('app', 'Required param "{name}" is missing', ['name' => 'task_id']));
        }

        if (($status = Yii::$app->request->post('status', null)) === null) {
            throw new BadRequestHttpException(Yii::t('app', 'Required param "{name}" is missing', ['name' => 'status']));
        }

        $json = Yii::$app->request->post('json', false);

        $task = Task::findOne($id);
        if (!$task) {
            throw new NotFoundHttpException(Yii::t('app', 'Task not found'));
        }

        if (!Yii::$app->getUser()->can('update_task', ['model' => $task])) {
            throw new ForbiddenHttpException(Yii::t('app', 'You can not update the task'));
        }

        if ($task->status == $status) {
            throw new BadRequestHttpException(Yii::t('app', 'Current status is same as new status'));
        }

        $action = null;
        switch ($status) {
            case Task::STATUS_NOT_COMPLETED:
                $action = new ToActual([], $task);
                break;
            case Task::STATUS_COMPLETED:
                $action = new ToCompleted([], $task);
                break;
            case Task::STATUS_POSTPONED:
                $action = new ToPostponed([], $task);
                break;
            default:
                throw new BadRequestHttpException(Yii::t('app', 'Unknown status'));
        }

        $actionError = $actionHistory = null;
        try {
            $action->run();
            $actionHistory = $action->historyRecord();
        } catch (ActionExcpetion $e) {
            $actionError = $e->getMessage();
        }

        return $this->prepareResponseAfterAction($task, $json, $actionError, $action, $actionHistory);
    }

    /**
     * @return string
     * @throws BadRequestHttpException
     * @throws ForbiddenHttpException
     * @throws Exception
     */
    public function actionDoAction()
    {

        $taskId = Yii::$app->request->post('task_id', null);
        if ($taskId === null) {
            throw new BadRequestHttpException(Yii::t('app', 'Required param "{name}" is missing', ['name' => 'task_id']));
        }

        $task = Task::findOne($taskId);
        if ($task === null) {
            throw new BadRequestHttpException(Yii::t('app', 'Unknown task'));
        }

        if (!Yii::$app->user->can('do_action', ['model' => $task])) {
            $viewData = $this->prepareViewData($task);
            return $this->renderAjax(
                'view', array_merge($viewData, ['action_error' => Yii::t('app', 'You do not have permission to do the action')])
            );
        }

        $actionClassName = Yii::$app->request->post('action_class', null);
        if ($actionClassName === null) {
            throw new BadRequestHttpException(Yii::t('app', 'Required param "{name}" is missing', ['name' => 'action_class']));
        }

        $json = Yii::$app->request->post('json', false);

        $actionClass = '\app\models\actions\\' . $actionClassName;
        if (!class_exists($actionClass)) {
            throw new BadRequestHttpException(Yii::t('app', 'Unknown action'));
        }
        $methods = [
            'getIntersectionNames',
            'run',
        ];
        foreach ($methods as $item) {
            if (!method_exists($actionClass, $item)) {
                throw new \app\models\actions\Exception(Yii::t('app', 'Action method "{method}" not found', ['method' => $item]));
            }
        }

        $taskGroupIntersectionNames = TaskTypeToTaskGroup::getIntersectionNamesByTaskGroupAndTaskType(
            $task->task_group_id, $task->task_type_id
        );
        $actionIntersectionNames = call_user_func("$actionClass::getIntersectionNames");
        if (count(array_intersect($taskGroupIntersectionNames, $actionIntersectionNames)) == 0) {
            throw new \app\models\actions\Exception(
                Yii::t(
                    'app',
                    'An action {action} does not match the group {group}',
                    ['action' => $actionClassName, 'group' => $task->task_group->name]
                )
            );
        }

        $optionalDate = Yii::$app->request->post('optional_date', null);

        $action = new $actionClass([], $task, $optionalDate !== null ? ['optional_date' => $optionalDate] : []);
        $actionError = null;
        $actionHistory = null;
        if ($action->validateParams()) {
            try {
                $action->run();
                $actionHistory = $action->historyRecord(Yii::$app->request->post('note', null));
            } catch (\app\models\actions\Exception $e) {
                $actionError = $e->getMessage();
            }
        } else {
            $actionError = Yii::t('app', 'Invalid params');
        }
        return $this->prepareResponseAfterAction($task, $json, $actionError, $action, $actionHistory);
    }

    /**
     * @param Task $task
     * @param bool $json
     * @param string $actionError
     * @param Action $action
     * @param ActionHistory $actionHistory
     * @return string
     */
    private function prepareResponseAfterAction($task, $json, $actionError, $action, $actionHistory)
    {
        if (!$json) {
            $viewData = $this->prepareViewData($task);
            return $this->renderAjax(
                'view', array_merge($viewData, ['action_error' => $actionError])
            );
        } else {
            $hasError = $actionError !== null;
            $response = [
                'success' => !$hasError,
            ];
            if ($hasError) {
                $response['message'] = $actionError;
            } else {
                $response['action_name'] = $action->getName();
                $response['action_history_id'] = $actionHistory->id;
                $response['task_id'] = $task->id;
                $response['task_name'] = $task->name;

                $actionHistory = ActionHistory::findOne($actionHistory->id);
                $response['action_history_created_at'] = $actionHistory->created_at;
            }
            return Json::encode($response);
        }
    }

    /**
     * @param int $group_id
     * @return string
     * @throws BadRequestHttpException
     * @throws ForbiddenHttpException
     */
    public function actionGetTaskTypes($group_id)
    {
        if (!Yii::$app->request->isAjax) {
            throw new BadRequestHttpException(Yii::t('app', 'The request should has ajax type'));
        }

        if (!Yii::$app->user->can('create_task')
            && !Yii::$app->user->can('update_task')
        ) {
            throw new ForbiddenHttpException(Yii::t('app', 'You can not get task types'));
        }

        return Json::encode(
            $this->getTaskTypesByGroupId($group_id)
        );
    }

    /**
     * @param int $group_id
     * @return array|\yii\db\ActiveRecord[]
     */
    protected function getTaskTypesByGroupId($group_id)
    {
        $taskTypesToTaskGroup = TaskTypeToTaskGroup::find()
            ->select(['task_type_id'])
            ->where(['task_group_id' => $group_id])
            ->all();

        $typeIds = [];
        foreach ($taskTypesToTaskGroup as $item) {
            $typeIds[] = $item->task_type_id;
        }

        return TaskType::find()
            ->select(['id', 'description'])
            ->andWhere(['in', 'id', $typeIds])
            ->all();
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param Task $task
     * @param bool $refresh
     * @return array
     * @throws ErrorException
     * @throws NotFoundHttpException
     */
    protected function prepareViewData(Task $task, $refresh = false)
    {
        if ($refresh) {
            $task->refresh();
        }

        // Check task name for six digit numbers
        $overlaps = [];
        if (preg_match_all('/[0-9]{6,}/', $task->name, $matches)) {
            $orders = Order::find()
                ->where(['in', 'number', $matches[0]])
                ->all();
            foreach ($orders as $o) {
                $overlaps[$o->number] = $o->status == 1 ? Yii::t('app', 'Ready') : Yii::t('app', 'Completed');
            }
        }

        // Get chat begin timestamp
        $createdAt = ChatMessage::getCreatedAtByIndexFromEnd(
            Yii::$app->params['chat_load_last_messages_count'], $task->id
        );
        $createdAtDt = $createdAt
            ? \DateTime::createFromFormat('Y-m-d H:i:s', $createdAt)
            : new \DateTime();

        return [
            'model' => $task,
            'overlaps' => $overlaps,
            'status_as_string' => Task::numericStatusToDisplayString($task->status),
            'task_group_list_items' => $this->prepareListItems(new TaskGroup()),
            'task_type_list_items' => $this->prepareListItems(
                $this->getTaskTypesByGroupId($task->task_group_id)
            ),
            'user_list_items' => Yii::$app->user->can('change_task_user')
                ? $this->prepareListItems(new User(), 'username')
                : null,
            'date_format' => Yii::$app->params['actual_to_date_format_php'],
            'chat_begin_timestamp' => $createdAtDt->format('U.u'),
            'chat_messages_limit' => Yii::$app->params['chat_load_last_messages_count'],
        ];
    }

    /**
     * @param mixed $data
     * @param string $text_attr
     * @return array
     * @throws ErrorException
     */
    protected function prepareListItems($data, $text_attr = 'description')
    {
        $list = [];
        $set = ($data instanceof \yii\db\ActiveRecord)
            ? $data->find()->all()
            : $data;
        foreach ($set as $current) {
            if (!isset($current->$text_attr)) {
                throw new ErrorException(Yii::t('app', 'Attribute not found: {attribute}', ['attribute' => $text_attr]));
            }
            $list[$current->id] = $current->$text_attr;
        }
        return $list;
    }

}
