<?php

namespace app\controllers;

use app\models\ChatMessageSearch;
use app\models\Task;
use Faker\Provider\zh_TW\DateTime;
use Yii;
use app\models\ChatMessage;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ChatController implements the CRUD actions for ChatMessage model.
 */
class ChatController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'read'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'read', 'load-previous'],
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'create' => ['post'],
                    'read' => ['get'],
                    'load-previous' => ['get'],
                ],
            ],
        ];
    }

    /**
     * Creates a new ChatMessage model.
     *
     * @return mixed
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        $taskId = Yii::$app->request->post('task_id', null);
        if ($taskId !== null) {
            if (($task = Task::findOne($taskId)) === null) {
                throw new NotFoundHttpException(Yii::t('app', 'The task is not found'));
            }
            if (!Yii::$app->user->can('send_message_to_task', ['model' => $task])) {
                throw new ForbiddenHttpException(Yii::t('app', 'You can not send message to the task'));
            }
        } else {
            if (!Yii::$app->user->can('send_message_to_main_chat')) {
                throw new ForbiddenHttpException(Yii::t('app', 'You can not send message to the main chat'));
            }
        }

        $model = new ChatMessage();
        if ($taskId !== null) {
            $model->task_id = $taskId;
        }
        $model->user_id = Yii::$app->request->post('user_id', null);
        $model->message = Yii::$app->request->post('message', null);

        if ($model->save()) {
            return Json::encode(['success' => true]);
        } else {
            return Json::encode(['success' => false, 'errors' => $model->getErrors()]);
        }
    }

    /**
     * @param int $first_message_id
     * @param int $task_id
     * @return string
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionLoadPrevious($first_message_id, $task_id = null)
    {
        $this->checkReadAccess($task_id);

        if (!ChatMessage::find()->where(['id' => $first_message_id])->exists()) {
            throw new NotFoundHttpException(Yii::t('app', 'The messages is not found'));
        }

        $messages = ChatMessageSearch::searchPrevious(
            $first_message_id, Yii::$app->params['chat_load_last_messages_count'], $task_id
        );

        return Json::encode(
            [
                'messages' => $this->prepareMessages($messages),
                'has_more' => count($messages) >= Yii::$app->params['chat_load_last_messages_count'],
            ]
        );
    }

    /**
     * @param string $from_timestamp
     * @param int $task_id
     * @return string
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     * @throws BadRequestHttpException
     */
    public function actionRead($from_timestamp, $task_id = null)
    {
        $this->checkReadAccess($task_id);

        $dt = \DateTime::createFromFormat('U.u', $from_timestamp);
        if ($dt === false) {
            throw new BadRequestHttpException('Can not parse timestamp');
        }

        $messages = ChatMessageSearch::search($dt->format('Y-m-d H:i:s'), $task_id);

        return Json::encode(
            [
                'timestamp' => (string)microtime(true),
                'messages' => $this->prepareMessages($messages),
            ]
        );
    }

    /**
     * @param int|null $task_id
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    private function checkReadAccess($task_id = null)
    {
        if ($task_id !== null) {
            if (($task = Task::findOne($task_id)) === null) {
                throw new NotFoundHttpException(Yii::t('app', 'The task is not found'));
            }

            if (!Yii::$app->user->can('read_messages_from_task', ['model' => $task])) {
                throw new ForbiddenHttpException(Yii::t('app', 'You can not read messages from the task'));
            }
        } else {
            if (!Yii::$app->user->can('read_messages_from_main_chat')) {
                throw new ForbiddenHttpException(Yii::t('app', 'You can not read messages from the main chat'));
            }
        }
    }

    /**
     * @param array $messages
     * @return array
     */
    protected function prepareMessages(array $messages)
    {
        $preparedMessages = [];
        foreach ($messages as $item) {
            $preparedMessages[] = [
                'id' => $item->id,
                'username' => $item->user->display_name . '(' . $item->user->username . ')',
                'user_id' => $item->user_id,
                'message' => $item->message,
                'created_at' => $item->created_at,
            ];
        }

        return $preparedMessages;
    }

    /**
     * Finds the ChatMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ChatMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ChatMessage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
