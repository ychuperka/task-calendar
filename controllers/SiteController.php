<?php

namespace app\controllers;

use app\models\ActionHistory;
use app\models\ActionHistorySearch;
use app\models\ChatMessage;
use app\models\TaskGroup;
use app\models\TaskType;
use app\models\User;
use Faker\Provider\zh_TW\DateTime;
use Yii;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\TaskSearch;
use app\models\Task;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'login', 'logout', 'day', 'week', 'completed'],
                'rules' => [
                    [
                        'actions' => ['index', 'logout', 'day', 'week', 'completed'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    private function putListsToViewData(array &$viewData, array $tasksIds)
    {
        $actionsHistories = ActionHistory::getListByTasksIds($tasksIds);
        $ahIds = [];
        foreach ($actionsHistories as $group) {
            foreach ($group as $ah) {
                $ahIds[] = $ah->id;
            }
        }

        $viewData['actions_histories'] = $actionsHistories;
        $viewData['task_groups_for_tasks'] = TaskGroup::getListByTasksIds($tasksIds);
        $viewData['task_types_for_tasks'] = TaskType::getListByTasksIds($tasksIds);
        $viewData['users_for_tasks'] = User::getListByTasksIds($tasksIds);
        $viewData['users_for_action_histories'] = User::getListByActionHistoriesIds($ahIds);
    }

    public function actionIndex()
    {
        $createdAt = ChatMessage::getCreatedAtByIndexFromEnd(
            Yii::$app->params['chat_load_last_messages_count']
        );
        $createdAtDt = $createdAt
            ? \DateTime::createFromFormat('Y-m-d H:i:s', $createdAt)
            : new \DateTime();
            

        $tasksData = $this->prepareTasksUsingCallbackAndTaskIds([$this, 'prepareTasksForADay']);
        $viewData = [
            'tasks' => $tasksData['tasks'],
            'task_groups' => TaskGroup::find()->all(),
            'chat_begin_timestamp' => $createdAtDt->format('U.u'),
            'chat_messages_limit' => Yii::$app->params['chat_load_last_messages_count'],
        ];
        $this->putListsToViewData($viewData, $tasksData['ids']);
        return $this->render('index', $viewData);
    }

    public function actionDay()
    {
        $tasksData = $this->prepareTasksUsingCallbackAndTaskIds([$this, 'prepareTasksForADay']);
        $viewData = [
            'tasks' => $tasksData['tasks'],
        ];
        $this->putListsToViewData($viewData, $tasksData['ids']);
        return $this->renderAjax('_day', $viewData);
    }

    /**
     * @param callback $callback
     *
     * @return array
     */
    private function prepareTasksUsingCallbackAndTaskIds($callback, $parameter = null)
    {
        $tasks = call_user_func($callback, $parameter);
        $tasksIds = [];
        foreach ($tasks as $group) {
            foreach ($group as $t) {
                $tasksIds[] = (int)$t->id;
            }
        }

        return [
            'tasks' => $tasks,
            'ids' => $tasksIds
        ];
    }

    public function actionWeek()
    {
        $tasksData = $this->prepareTasksUsingCallbackAndTaskIds([$this, 'prepareTasksForAWeek']);
        $viewData = [
            'tasks' => $tasksData['tasks'],
        ];
        $this->putListsToViewData($viewData, $tasksData['ids']);
        return $this->renderAjax('_week', $viewData);
    }

    public function actionWeekActionsPerformed()
    {
        $tasksData = $this->prepareTasksUsingCallbackAndTaskIds([$this, 'prepareTasksForAWeek'], true);
        $viewData = [
            'tasks' => $tasksData['tasks'],
            'actions_performed' => true
        ];
        $this->putListsToViewData($viewData, $tasksData['ids']);
        return $this->renderAjax('_week', $viewData);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    protected function prepareTasksForAWeek($actions_performed = false)
    {
        $userId = Yii::$app->user->can('view_task')
            ? null
            : Yii::$app->user->id;

        $request = Yii::$app->request;
        $name = $request->post('name');
        if (is_string($name)) {
            $name = trim($name);
        }
        $date = $request->post('date');
        $taskGroupId = $request->post('task_group_id', null);

        if ($date !== null) {
            $date = $this->convertDate($date);
            if ($date !== null) {

                $dateDt = new \DateTime($date);
                $dateEndDt = clone $dateDt;
                $dateEndDt = $dateEndDt->add(\DateInterval::createFromDateString('1 weeks'));

                $dateEnd = $dateEndDt->format('Y-m-d');

                unset($dateDtClone);
            }
        }
        if ($date === null) {

            $dateDt = new \DateTime();
            $dateEndDt = clone $dateDt;
            $dateEndDt->add(\DateInterval::createFromDateString('1 weeks'));

            $date = $dateDt->format('Y-m-d');
            $dateEnd = $dateEndDt->format('Y-m-d');
        }

        // Prepare query
        if (!$actions_performed) {
            $query = Task::find()
                ->filterWhere(
                    [
                        'or',
                        ['task.user_id' => $userId],
                        ['for_all' => $userId !== null ? 1 : null],
                    ]
                )->andWhere(
                    '(actual_to >= :date AND actual_to < :date_end) OR actual_to IS NULL',
                    [
                        ':date' => $date,
                        ':date_end' => $dateEnd,
                    ]
                )->andWhere(['status' => Task::STATUS_NOT_COMPLETED]);
        } else {
            $query = Task::find()
                ->joinWith('actionHistories')
                ->filterWhere(
                    [
                        'or',
                        ['task.user_id' => $userId],
                        ['task.for_all' => $userId !== null ? 1 : null],
                    ]
                )
                ->andWhere(
                    [
                        'and',
                        ['>=', 'action_history.created_at', $date],
                        ['<', 'action_history.created_at', $dateEnd],
                    ]
                );
        }
        $query->andFilterWhere(['like', 'task.name', $name])
            ->andFilterWhere(['task.task_group_id' => $taskGroupId]);

        $preparedTasks = [];
        $models = $query->all();
        do {
            $currentDayTasks = [];
            $date = $dateDt->format('Y-m-d');

            if (!$actions_performed) {
                foreach ($models as $task) {
                    if ($task->actual_to === null) {
                        $currentDayTasks[] = $task;
                        continue;
                    }
                    $taskDt = \DateTime::createFromFormat(
                        Yii::$app->params['actual_to_date_format_php'], $task->actual_to
                    );
                    if ($taskDt->format('Y-m-d') === $date) {
                        $currentDayTasks[] = $task;
                    }
                }
            } else {

                /*
                 * Not so fast, but works.
                 * TODO: Make optimization, may be refactoring
                 */
                // For each query result item do
                foreach ($models as $task) {
                    // For each action history record do
                    foreach ($task->actionHistories as $ah) {
                        // Compare action history record creation date and current day date
                        $ahDt = new \DateTime($ah->created_at);
                        if ($ahDt->format('Y-m-d') == $date) {
                            $currentDayTasks[] = $task;
                            continue 2;
                        }
                    }
                }

                /*
                 * The closure help us to find last ActionHistory instance
                 * created at specified date
                 */
                $findLatestAhForDate = function (array $list, \DateTime $dt) {
                    $count = count($list);
                    for ($i = $count - 1; $i >= 0; --$i) {
                        $ahDt = new \DateTime($list[$i]->created_at);
                        if ($ahDt->diff($dt)->days == 0) {
                            return $list[$i];
                        }
                    }
                    return false;
                };
                /*
                 * Sort tasks list
                 */
                usort($currentDayTasks, function ($a, $b) use ($findLatestAhForDate, $dateDt) {

                    /*
                     * The results are inverted cause we want DESC order
                     */

                    // Latest ah for the "a" is not found (like a < b)
                    $firstLatestAh = $findLatestAhForDate($a->actionHistories, $dateDt);
                    if (!$firstLatestAh) {
                        return 1;
                    }

                    // Latest ah for the "b" is not found (like a > b)
                    $secondLatestAh = $findLatestAhForDate($b->actionHistories, $dateDt);
                    if (!$secondLatestAh) {
                        return -1;
                    }

                    // Compare dates
                    $aDt = new \DateTime($firstLatestAh->created_at);
                    $bDt = new \DateTime($secondLatestAh->created_at);
                    if ($aDt > $bDt) {
                        return -1;
                    } else if ($aDt < $bDt) {
                        return 1;
                    } else {
                        return 0;
                    }
                });
            }

            $preparedTasks[$date] = $currentDayTasks;
            $dateDt->add(\DateInterval::createFromDateString('1 days'));

        } while ($dateDt->diff($dateEndDt)->days > 0);


        return $preparedTasks;
    }

    /**
     * @return array
     */
    protected function prepareTasksForADay()
    {
        $user_id = Yii::$app->user->can('view_task')
            ? null
            : Yii::$app->user->id;

        $buildQueryBeginning = function () use ($user_id) {
            return Task::find()
                ->filterWhere(
                    ['or',
                        ['task.user_id' => $user_id],
                        ['for_all' => $user_id !== null ? 1 : null],
                    ]
                )->orderBy('actual_to desc');
        };

        // Get actual tasks
        $request = Yii::$app->request;
        $date = $request->post('date');
        $name = $request->post('name');
        if (is_string($name)) {
            $name = trim($name);
        }
        $taskGroupId = $request->post('task_group_id');
        if ($date !== null) {
            $date = $this->convertDate($date);
        } else {
            $date = date('Y-m-d');
        }

        $tasksFiltered = [];
        $putModelsToArrayFunc = function (array $models, $key) use (&$tasksFiltered) {
            foreach ($models as $m) {
                $tasksFiltered[$key][] = $m;
            }
        };

        if ($name === null) {
            $query = $buildQueryBeginning()
                ->andWhere(
                    [
                        'or',
                        ['actual_to' => null],
                        ['actual_to' => $date !== null ? $date : date('Y-m-d')],
                    ]
                )
                ->andWhere(['status' => Task::STATUS_NOT_COMPLETED])
                ->andFilterWhere(['task_group_id' => $taskGroupId]);

            $putModelsToArrayFunc($query->all(), 'actual');
        }


        // Get not completed tasks
        $query = $buildQueryBeginning()
            ->andWhere(['status' => Task::STATUS_NOT_COMPLETED])
            ->andFilterWhere(['like', 'name', $name])
            ->andFilterWhere(['task_group_id' => $taskGroupId]);
        if ($name === null) {
            $query->andWhere(['<', 'actual_to', $date]);
        }
        $putModelsToArrayFunc($query->all(), 'not_completed');

        // Get postponed tasks
        $query = $buildQueryBeginning()
            ->andWhere(['status' => Task::STATUS_POSTPONED])
            ->andFilterWhere(['like', 'name', $name])
            ->andFilterWhere(['task_group_id' => $taskGroupId]);
        $putModelsToArrayFunc($query->all(), 'postponed');

        // Get completed tasks
        $query = $buildQueryBeginning()
            ->joinWith('actionHistories')
            ->andWhere(['status' => Task::STATUS_COMPLETED])
            ->andFilterWhere(['like', 'name', $name])
            ->andFilterWhere(['task_group_id' => $taskGroupId])
            ->orderBy('action_history.created_at desc')
            ->limit(Yii::$app->params['completed_tasks_limit']);
        $putModelsToArrayFunc($query->all(), 'completed', Yii::$app->params['completed_tasks_limit']);

        return $tasksFiltered;
    }

    /**
     * @param string $date
     * @return null|string
     */
    protected function convertDate($date)
    {
        $dt = \DateTime::createFromFormat(Yii::$app->params['actual_to_date_format_php'], $date);
        if ($dt === false) {
            return null;
        }

        return $dt->format('Y-m-d');
    }
}
