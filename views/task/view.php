<?php

use yii\base\Widget;
use trntv\yii\datetime\DateTimeWidget;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $task_group_list_items array */
/* @var $task_type_list_items array */
/* @var $user_list_items array */
/* @var $status_as_string */
/* @var $action_error  string */
/* @var $overlaps array */
/* @var $chat_begin_timestamp string */
/* @var $chat_messages_limit int */

$this->title = $model->name;
Widget::$autoIdPrefix = 'task-edit-w';
?>
<div class="task-view">
    <div class="row">
        <div class="col-xs-6">
            <!-- Task info -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="list-group">
                        <!-- Task info -->
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading"><?= Yii::t('app', 'Task Info') ?></h4>
                            <p class="list-group-item-text">
                                <span id="task-status"><?= Yii::t('app', 'Status') ?>: <?= $status_as_string ?></span><br />
                                <span id="task-created-at"><?= Yii::t('app', 'Created At') ?>: <?= \DateTime::createFromFormat('Y-m-d H:i:s', $model->created_at)->format('d.m.Y H:i'); ?></span><br />
                                <span id="task-creator"><?= Yii::t('app', 'Creator') ?>: <?= $model->creator->display_name ?></span>
                            </p>
                        </div>
                        <?php if (count($overlaps) > 0): ?>
                        <!-- Overlaps -->
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading"><?= Yii::t('app', 'Overlaps') ?></h4>
                            <p class="list-group-item-text">
                                <?php foreach ($overlaps as $number => $status): ?>
                                    <span><?= $number ?>: <?= $status ?></span>
                                <?php endforeach; ?>
                            </p>
                        </div>
                        <?php endif; ?>
                        <!-- Manage -->
                        <?= $this->render('_manage', ['model' => $model, 'show_create_double' => true]) ?>
                    </div>
                </div>
            </div>
            <!--Add new Action -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <?php
                            echo $this->render(
                                '_add_new_action.php',
                                [
                                    'action_error' => isset($action_error) ? $action_error : null,
                                    'model' => $model,
                                ]
                            );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <!-- Task edit form -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3><?= Yii::t('app', 'Task Edit') ?></h3>
                            <?php
                            echo $this->render(
                                'create',
                                [
                                    'model' => $model,
                                    'task_group_list_items' => $task_group_list_items,
                                    'task_type_list_items' => $task_type_list_items,
                                    'user_list_items' => $user_list_items,
                                ]
                            );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Chat window -->
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    echo $this->render(
                        '../site/_chat',
                        [
                            'chat_begin_timestamp' => $chat_begin_timestamp,
                            'chat_messages_limit' => $chat_messages_limit,
                            'chat_id' => 'task',
                            'task_id' => $model->id,
                        ]
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Action History -->
    <?php if (count($model->actionHistories) > 0): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4><?= Yii::t('app', 'Action History') ?></h4>
                        <table id="action-history" class="table table-hover">
                            <thead>
                            <tr>
                                <td><?= Yii::t('app', 'Created At') ?></td>
                                <td><?= Yii::t('app', 'Action') ?></td>
                                <td><?= Yii::t('app', 'Date') ?></td>
                                <td><?= Yii::t('app', 'By Whom') ?></td>
                                <td><?= Yii::t('app', 'Note') ?></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($model->actionHistories as $item): ?>
                                <tr>
                                    <td><?= $item->created_at ?></td>
                                    <td><?= \app\models\Action::convertClassNameToActionName($item->action_class) ?></td>
                                    <td><?= $item->optional_datetime ?></td>
                                    <td><?= $item->user->display_name ?></td>
                                    <td><?= nl2br($item->note) ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<script type="text/javascript">
    (function($) {

        /**
         * Loads modal window content, used in form submit handlers
         */
        function loadModalWindowContent(theForm) {
            var modalWindowBlock = $("div#modal");
            var modalWindowBlockContentBlock = modalWindowBlock.find("div#modal-content");
            modalWindowBlockContentBlock.html("<div class=\"loader-container\"><img src=\"/img/loader.gif\" alt=\"Loading...\" /></div>");

            $.ajax({
                url: theForm.attr("action"),
                method: theForm.attr("method"),
                dataType: "html",
                data: theForm.serialize(),
                success: function (response) {
                    modalWindowBlockContentBlock.html(response);
                },
                error: function (response) {
                    modalWindowBlockContentBlock.html(response.responseText);
                }
            });
        }

        /*
        Set status
        */
        var setStatusForm = $("form.form-set-status");
        setStatusForm.submit(function(event) {
            event.preventDefault();
            loadModalWindowContent($(this));
            return false;
        });

        /*
         Double create
         */
        var doubleCreateForm = $("form#form-create-double");
        doubleCreateForm.submit(function(event) {
            event.preventDefault();
            loadModalWindowContent($(this));
            return false;
        });

        // Do Action
        var actionForm = $("form.form-action-do"),
            actionList = actionForm.find("select#action-list"),
            actionFormDateInput = actionForm.find("input#optional-date"),
            actionFormNoteInput = actionForm.find("input#action-note"),
            actionFormSubmitButton = actionForm.find("button[type=submit]"),
            actionHistoryTable = $("table#action-history");
        actionForm.submit(function(event) {
            event.preventDefault();

            /*
            if (!confirm("<?= Yii::t('app', 'Are you sure?') ?>")) {
                return;
            }
            */

            function toggleControls() {
                actionList.prop("disabled", !actionList.prop("disabled"));
                actionFormDateInput.prop("disabled", !actionFormDateInput.prop("disabled"));
                actionFormNoteInput.prop("disabled", !actionFormNoteInput.prop("disabled"));
                actionFormSubmitButton.prop("disabled", !actionFormSubmitButton.prop("disabled"));
            }


            var modalWindowBlock = $("div#modal");
            var modalWindowBlockContentBlock = modalWindowBlock.find("div#modal-content");
            modalWindowBlockContentBlock.html("<div class=\"loader-container\"><img src=\"/img/loader.gif\" alt=\"Loading...\" /></div>");

            var theForm = $(this);
            var dataToSend = theForm.serialize();
            toggleControls();

            $.ajax({
                url: theForm.attr("action"),
                method: theForm.attr("method"),
                dataType: "html",
                data: dataToSend,
                success: function (response) {
                    modalWindowBlockContentBlock.html(response);
                },
                error: function (response) {
                    modalWindowBlockContentBlock.html(response.responseText);
                }
            });

            return false;
        });

    })(jQuery);

</script>