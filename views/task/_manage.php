<?php
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $show_create_double bool */
?>
<div class="list-group-item">
    <h4 class="list-group-item-heading"><?= Yii::t('app', 'Manage') ?></h4>
    <?php if (isset($show_create_double) && $show_create_double): ?>
    <div class="list-group-item-text">
        <?= Html::beginForm(Url::toRoute('/task/create-double'), 'POST', ['id' => 'form-create-double']) ?>
        <?= Html::hiddenInput('task_id', $model->id) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Create a Double'), ['class' => 'btn btn-warning']) ?>
        </div>
        <?= Html::endForm(); ?>
    </div>
    <?php endif; ?>
    <?php if ($model->status != \app\models\Task::STATUS_COMPLETED): ?>
        <div class="list-group-item-text">
            <?= Html::beginForm(Url::toRoute('/task/set-status'), 'POST', ['class' => 'form-set-status']) ?>
            <?= Html::hiddenInput('task_id', $model->id) ?>
            <?= Html::hiddenInput('status', \app\models\Task::STATUS_COMPLETED) ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'To Completed'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= Html::endForm(); ?>
        </div>
    <?php endif; ?>
    <?php if ($model->status != \app\models\Task::STATUS_POSTPONED): ?>
        <div class="list-group-item-text">
            <?= Html::beginForm(Url::toRoute('/task/set-status'), 'POST', ['class' => 'form-set-status']) ?>
            <?= Html::hiddenInput('task_id', $model->id) ?>
            <?= Html::hiddenInput('status', \app\models\Task::STATUS_POSTPONED) ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'To Postponed'), ['class' => 'btn btn-primary']) ?>
            </div>
            <?= Html::endForm(); ?>
        </div>
    <?php endif; ?>
    <?php if ($model->status != \app\models\Task::STATUS_NOT_COMPLETED): ?>
        <div class="list-group-item-text">
            <?= Html::beginForm(Url::toRoute('/task/set-status'), 'POST', ['class' => 'form-set-status']) ?>
            <?= Html::hiddenInput('task_id', $model->id) ?>
            <?= Html::hiddenInput('status', \app\models\Task::STATUS_NOT_COMPLETED) ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'To Actual'), ['class' => 'btn']) ?>
            </div>
            <?= Html::endForm(); ?>
        </div>
    <?php endif; ?>
</div>