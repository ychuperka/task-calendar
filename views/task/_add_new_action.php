<?php
use yii\helpers\Html;
use yii\helpers\Url;
use trntv\yii\datetime\DateTimeWidget;

/** @var $action_error string */
/** @var $model \app\models\Task */
/** @var $optional_date string */
?>
    <h4><?= Yii::t('app', 'Add new Action') ?></h4>
<?php if ($action_error && strlen($action_error) > 0): ?>
    <div class="alert alert-danger">
        <?= nl2br($action_error) ?>
    </div>
<?php endif; ?>
<?php
echo Html::beginForm(Url::toRoute('/task/do-action'), 'POST', ['class' => 'form-action-do']);
?>
<?= Html::hiddenInput('task_id', $model->id) ?>
    <div class="form-group">
        <?= Html::label(Yii::t('app', 'Action'), 'action-list') ?>
        <?= Html::dropDownList('action_class', null, $model->getActionClassesAsListItems(), ['class' => 'form-control', 'id' => 'action-list']) ?>
    </div>
    <div class="form-group">
        <?= Html::label(Yii::t('app', 'Note'), 'action-message') ?>
        <?= Html::input('text', 'note', null, ['class' => 'form-control', 'id' => 'action-note']) ?>
    </div>
    <div class="form-group">
        <?= Html::label(Yii::t('app', 'Date'), 'action-date') ?>
        <?php
        echo DateTimeWidget::widget(
            [
                'name' => 'optional_date',
                'options' => [
                    'id' => 'optional-date',
                ],
                'momentDatetimeFormat' => Yii::$app->params['actual_to_date_format_moment'],
                'clientOptions' => [
                    'allowInputToggle' => true
                ]
            ]
        );
        ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Add'), ['class' => 'btn btn-primary']) ?>
    </div>
<?= Html::endForm() ?>