<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use trntv\yii\datetime\DateTimeWidget;
use yii\base\Widget;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
/* @var $task_group_list_items array */
/* @var $task_type_list_items array */
/* @var $user_list_items array|null */
/* @var $actual_to string */
/* @var $date_format string */



$can_update = $model->isNewRecord || Yii::$app->user->can('update_task', ['model' => $model]);
?>

<div class="task-form">

    <?php  Widget::$autoIdPrefix = 'task-create-w'; ?>

    <?php
    $form = ActiveForm::begin(
        [
            'id' => 'form-task-create',
            'action' => $model->isNewRecord ? Url::toRoute('/task/create') : Url::toRoute(['/task/update', 'id' => $model->id]),
            'method' => 'POST',
        ]
    );
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => !$can_update]) ?>

    <?= $form->field($model, 'task_group_id')->listBox($task_group_list_items, ['id' => 'task-groups', 'disabled' => !$can_update, 'size' => 6]) ?>

    <?php $isTaskTypesListDisabled = count($task_type_list_items) == 0 || !$can_update; ?>
    <?= $form->field($model, 'task_type_id')->listBox($task_type_list_items, ['id' => 'task-types', 'disabled' => $isTaskTypesListDisabled, 'size' => 6]) ?>

    <?= $form->field($model, 'for_all')->checkbox(['id' => 'for-all', 'disabled' => !$can_update]) ?>

    <?php
    $options = [
        'id' => 'date-selector',
        'placeholder' => $model->isNewRecord
            ? (new \DateTime())->format(Yii::$app->params['actual_to_date_format_php'])
            : '',
        'disabled' => !$can_update,
    ];
    if (isset($actual_to) && $actual_to != null) {
        $options['value'] = Yii::$app->formatter->asDate($actual_to, 'php:' . $date_format);
    }
    echo $form->field($model, 'actual_to')->widget(
        DateTimeWidget::className(),
        [
            'options' => $options,
            'phpDatetimeFormat' => 'php:' . Yii::$app->params['actual_to_date_format_php'],
            'momentDatetimeFormat' => Yii::$app->params['actual_to_date_format_moment'],
            'clientOptions' => [
                'allowInputToggle' => true
            ]
        ]
    )
    ?>

    <?php if ($user_list_items !== null): ?>
        <?= $form->field($model, 'user_id')->listBox($user_list_items, ['id' => 'user-list', 'disabled' => !$can_update]) ?>
    <?php endif; ?>

    <?php if ($can_update): ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id' => 'submit', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php endif; ?>

    <?php ActiveForm::end(); ?>
</div>
<script type="text/javascript">
    (function($) {
        /*
         If submit button is clicked then send form`s data via ajax request and
         prevent native request.
         */
        var submitButton = $("form#form-task-create button[type=submit]");
        var modalWindowBlock = $("div#modal");
        var modalWindowBlockContentBlock = modalWindowBlock.find("div#modal-content");
        $("form#form-task-create").on('beforeSubmit', function(event) {

            var form = $(this);
            submitButton.prop("disabled", true);

            var dataToSend = form.serialize();

            modalWindowBlockContentBlock.html("<div class=\"loader-container\"><img src=\"/img/loader.gif\" alt=\"Loading...\" /></div>");

            $.ajax({
                url: form.attr("action"),
                method: form.attr("method"),
                dataType: "json",
                data: dataToSend,
                success: function(response) {
                    modalWindowBlock.modal("hide");
                },
                error: function(response) {
                    modalWindowBlockContentBlock.html(response.responseText);
                }
            });

            return false;
        });

        /*
         If task groups list value changes then request
         task types assigned to the selected task group.
         */
        var taskTypesListBox = $("select#task-types");
        $("select#task-groups").change(function(event) {

            var theListBox = $(this);
            var groupId = theListBox.val();

            submitButton.prop("disabled", true);
            theListBox.prop("disabled", true);
            taskTypesListBox.prop("disabled", true);

            $.ajax({
                url: "<?= Url::toRoute('/task/get-task-types') ?>",
                method: "GET",
                dataType: "json",
                data: {group_id: groupId},
                success: function(response) {

                    taskTypesListBox.html("");
                    var firstVal = null;
                    for (var i = 0; i < response.length; ++i) {
                        var item = response[i];
                        if (firstVal === null) {
                            firstVal = item.id;
                        }
                        var option = $("<option>").attr("value", item.id).html(item.description);
                        taskTypesListBox.append(option);
                    }
                    taskTypesListBox.val(firstVal);

                    submitButton.prop("disabled", false);
                    theListBox.prop("disabled", false);
                    taskTypesListBox.prop("disabled", false);
                },
                error: function(response) {
                    modalWindowBlockContentBlock.html(response.responseText);
                }
            });
        });
    })(jQuery);

    /*
    Hide users list if checkbox "For All" is checked or show it if no
     */
    (function($) {

        var userList = $("select#user-list");
        var checkbox = $("input#for-all[type=checkbox]");
        <?php if ($model->isNewRecord && count($model->errors) == 0 && Yii::$app->user->can('view_task')): ?>
        checkbox.prop("checked", true);
        <?php endif; ?>
        if (userList.length > 0) {
            if (checkbox.is(":checked")) {
                userList.parent().hide();
            }

            checkbox.change(function(event) {
                debugger;
                var theCheckbox = $(this);
                if (theCheckbox.is(":checked")) {
                    userList.parent().hide("fast");
                    userList.val("@");
                } else {
                    userList.parent().show("fast");
                }
            });
        }

    })(jQuery);

    /*
    If editing task, show submit button only if edit form is changed
     */
    (function($) {

        var submitButton = $("button#submit");
        <?php if (!$model->isNewRecord): ?>
        submitButton.hide();
        <?php endif; ?>

        $("form#form-task-create input, form#form-task-create select").on("change keypress", function(event) {
            submitButton.show("fast");
        });

        $("form#form-task-create input#date-selector").click(function(event) {
            submitButton.show("fast");
        });

    })(jQuery);

    (function($) {
        /*
         Focus on task name field on load
         */
        var i = setInterval(function() {
            var field = $("input#task-name");
            if (field.length == 0) {
                return;
            }
            field.focus();
            clearInterval(i);
        }, 500);
    })(jQuery);
</script>
