<?php
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $chat_begin_timestamp string */
/* @var $chat_messages_limit int */
/* @var $task_id int|null */
/* @var $chat_id string */


$uid = Yii::$app->user->id;
$this->registerJs(
    !isset($task_id) ? 'new Chat(' . $uid . ', "div#chat-' . $chat_id . '");'
        : 'new Chat(' . $uid . ', "div#chat-' . $chat_id . '", ' . $task_id . ');',
    \yii\web\View::POS_READY
);
?>
<div id="chat-<?= $chat_id ?>" class="panel panel-info">
    <div class="panel-heading">
        <?= Yii::t('app', 'Chat') ?>
        <?php if (!isset($task_id)): ?>
        <div class="pull-right">
            <i class="glyphicon glyphicon-menu-down cursor-pointer chat-expander"></i>
        </div>
        <?php endif; ?>
    </div>
    <div class="panel-body chat-container"<?php if(!isset($task_id)): ?> style="display: none;"<?php endif; ?>>
        <div data-timestamp="<?= $chat_begin_timestamp ?>"
             data-url="<?= Url::toRoute('/chat/read') ?>"
             data-user-id="<?= Yii::$app->user->id ?>"
             class="well well-sm chat-contents-container">
            <div class="center-block" style="text-align: center;">
                <img alt="<?= Yii::t('app', 'Loading...') ?>" src="/img/loader_mini.gif" style="display: none;" />
                <a class="previous" href="<?= Url::toRoute('/chat/load-previous') ?>">
                    <?= Yii::t('app', 'Load previous messages') ?>
                    <i class="glyphicon glyphicon-menu-up"></i>
                </a>
            </div>
        </div>
        <form class="form-chat" action="<?= Url::toRoute('/chat/create') ?>" method="POST"
              enctype="application/x-www-form-urlencoded">
            <div class="form-group">
                <label for="chat-message" class="control-label"><?= Yii::t('app', 'Message') ?></label>
                <input class="form-control" id="chat-message" name="message" type="text" />
            </div>
            <input type="hidden" name="user_id" value="<?= Yii::$app->user->id ?>" />
                <button type="submit" class="btn btn-primary"><?= Yii::t('app', 'Send') ?></button>
        </form>
    </div>
</div>