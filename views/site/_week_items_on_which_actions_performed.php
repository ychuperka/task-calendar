<?php
use yii\helpers\Url;
use app\models\Task;

/* @var array $task_groups_for_tasks */
/* @var array $task_types_for_tasks */
/* @var array $actions_histories */
/* @var array $users_for_tasks */
/* @var $users_for_action_histories */

$this->registerJsFile('@web/js/expander.js', ['depends' => \yii\web\JqueryAsset::className()]);
?>
<?php
$currentDt = \DateTime::createFromFormat('Y-m-d', $date);
$dateAsTimestamp = strtotime($date);
$isToday = $currentDt->diff(new DateTime())->days == 0;
?>

<div class="col-xs-4">
    <div class="panel panel-default week-item<?php if ($isToday): ?> week-item-today<?php endif; ?>">
        <div class="panel-body">
            <h3><?= Yii::$app->formatter->asDate($date, 'php:D, d/m'); ?></h3>
            <?php foreach ($items as $t): ?>
                <?php
                $task_type = $task_types_for_tasks[$t->id];
                $task_group = $task_groups_for_tasks[$t->id];
                $task_user = isset($users_for_tasks[$t->id]) ? $users_for_tasks[$t->id] : null;;
                ?>
                <div class="panel panel-default
                    task-status-<?= Task::numericStatusToString($t->status) ?>
                    task-group-<?= $task_group['name'] ?>
                    task-type-<?= $task_type['name'] ?><?php if($task_user): ?>
                    task-performer-<?= $task_user['username'] ?><?php endif; ?><?php if ($t->for_all): ?>
                    for-all<?php endif; ?>"
                     data-task-id="<?= $t->id ?>">
                    <div class="panel-body">
                        <div class="pull-left">
                            <h4><?= $t->name ?></h4>
                            <h5>(<?= $task_group['description'] ?> &rarr; <?= $task_type['description'] ?>)</h5>
                        </div>
                        <div class="pull-right">
                            <i class="glyphicon glyphicon-menu-down cursor-pointer expander"
                               data-block-id="actions-container-<?= $dateAsTimestamp ?>-<?= $t->id ?>"></i>
                            &nbsp;
                            <i class="glyphicon glyphicon-pencil modal-opener cursor-pointer"
                               data-task-id="<?= $t->id ?>"
                               data-modal-title="<?= $t->name; ?>"
                               data-modal-url="<?= Url::toRoute(['/task/view', 'id' => $t->id]) ?>"></i>
                        </div>
                        <?php if (count($actions_histories) > 0): ?>
                            <div class="clearfix"></div>
                            <ul class="list-group actions-container" id="actions-container-<?= $dateAsTimestamp ?>-<?= $t->id ?>">
                                <?php foreach ($actions_histories[$t->id] as $ah): ?>
                                    <?php
                                    $ahCreatedAtDt = \DateTime::createFromFormat('Y-m-d H:i:s', $ah->created_at);
                                    $shouldMakeExpandableNote = $ah->action_class == 'Created'
                                        || $ah->action_class == 'Modified';
                                    $hasOptDt = (bool)$ah->optional_datetime;
                                    $user = $users_for_action_histories[$ah->id];
                                    ?>
                                    <?php if ($currentDt->diff($ahCreatedAtDt)->days != 0) { continue; }?>
                                    <li class="list-group-item">
                                        <div class="pull-left">
                                            <b><?= $user['display_name']; ?></b>:
                                            <?= \app\models\Action::convertClassNameToActionName($ah->action_class) ?>
                                            (<?= Yii::$app->formatter->asDate($ah->created_at, 'php:d.m.y H:i') ?>)
                                            <?php if ($hasOptDt): ?>
                                                , <?= Yii::t('app', 'Date') ?>: <?= Yii::$app->formatter->asDate($ah->optional_datetime, 'php:d.m.Y') ?>
                                            <?php endif; ?>
                                            <?php if (!$shouldMakeExpandableNote && $ah->note): ?>
                                                , <?= Yii::t('app', 'Note') ?>: <?= $ah->note ?>
                                            <?php endif; ?>
                                        </div>
                                        <?php if ($shouldMakeExpandableNote && $ah->note): ?>
                                            <div class="pull-right">
                                                <i data-block-id="action-note-<?= $dateAsTimestamp ?>-<?= $ah->id ?>"
                                                   class="expander glyphicon glyphicon-menu-down cursor-pointer"></i>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div id="action-note-<?= $dateAsTimestamp ?>-<?= $ah->id ?>"
                                                 class="well well-sm note-hidden">
                                                <p>
                                                    <?php if ($ah->optional_datetime): ?>
                                                        <?= Yii::t('app', 'Date') ?>: <?= Yii::$app->formatter->asDate($ah->optional_datetime, 'php:d.m.Y') ?>
                                                        <br/>
                                                    <?php endif; ?>
                                                    <?= nl2br($ah->note) ?>
                                                </p>
                                            </div>
                                        <?php else: ?>
                                            <div class="clearfix"></div>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
