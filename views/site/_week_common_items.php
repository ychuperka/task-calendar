<?php
/* @var array $task_groups_for_tasks */
/* @var array $task_types_for_tasks */
/* @var array $actions_histories */
/* @var array $users_for_tasks */
/* @var array $users_for_action_histories */

use yii\helpers\Url;
use yii\base\Widget;
use kartik\popover\PopoverX;

$this->registerJsFile('@web/js/expander.js', ['depends' => \yii\web\JqueryAsset::className()]);

$oldPrefix = Widget::$autoIdPrefix;
Widget::$autoIdPrefix = '_week_common_items_w';
?>
<?php
$currentDt = \DateTime::createFromFormat('Y-m-d', $date);
$dateAsTimestamp = strtotime($date);
$isToday = $currentDt->diff(new DateTime())->days == 0;
?>
    <div class="col-xs-4">
        <div class="panel panel-default week-item<?php if ($isToday): ?> week-item-today<?php endif; ?>">
            <div class="panel-heading">
                <h4><?= Yii::$app->formatter->asDate($date, 'php:D, d/m'); ?></h4>
            </div>
            <div class="panel-body">
                <div class="list-group">
                    <button type="button" class="list-group-item list-group-item-success modal-opener"
                            data-modal-url="<?= Url::toRoute(['/task/create', 'actual_to' => $date]) ?>"
                            data-modal-title="<?= Yii::t('app', 'Add New') ?>">
                        <?= Yii::t('app', 'Add New') ?>
                    </button>
                    <?php foreach ($items as $t): ?>
                        <?php
                        $task_type = $task_types_for_tasks[$t->id];
                        $task_group = $task_groups_for_tasks[$t->id];
                        $task_user = isset($users_for_tasks[$t->id]) ? $users_for_tasks[$t->id] : null;;
                        ?>
                        <div
                            class="list-group-item task-status-<?= \app\models\Task::numericStatusToString($t->status) ?>
                            task-group-<?= $task_group['name'] ?>
                            task-type-<?= $task_type['name'] ?><?php if ($task_user): ?>
                            task-performer-<?= $task_user['username'] ?><?php endif; ?><?php if ($t->for_all): ?>
                            for-all<?php endif; ?>">
                            <span><?= $t->name; ?>
                                (<?= $task_group['description'] ?> &rarr; <?= $task_type['description'] ?>)</span>

                            <div class="pull-right">
                                <i class="glyphicon glyphicon-menu-down cursor-pointer expander"
                                   data-block-id="actions-container-<?= $dateAsTimestamp ?>-<?= $t->id ?>"></i>
                                &nbsp;
                                <i class="glyphicon glyphicon-pencil modal-opener cursor-pointer"
                                   data-task-id="<?= $t->id ?>"
                                   data-modal-title="<?= $t->name; ?>"
                                   data-modal-url="<?= Url::toRoute(['/task/view', 'id' => $t->id]) ?>"></i>&nbsp;
                                <?php
                                echo PopoverX::widget(
                                    [
                                        'header' => Yii::t('app', 'Manage'),
                                        'class' => 'manage',
                                        'placement' => PopoverX::ALIGN_RIGHT,
                                        'type' => PopoverX::TYPE_INFO,
                                        'content' => $this->render('../task/_manage', ['model' => $t]),
                                        'toggleButton' => [
                                            'tag' => 'i',
                                            'label' => '',
                                            'class' => 'glyphicon glyphicon-flag cursor-pointer'
                                        ],
                                    ]
                                );
                                ?>
                                &nbsp;
                                <?php
                                echo PopoverX::widget(
                                    [
                                        'header' => Yii::t('app', 'Action'),
                                        'placement' => PopoverX::ALIGN_RIGHT,
                                        'type' => PopoverX::TYPE_SUCCESS,
                                        'options' => [
                                            'style' => 'width:300px'
                                        ],
                                        'content' => $this->render(
                                            '../task/_add_new_action',
                                            [
                                                'model' => $t,
                                                'action_error' => isset($action_error) ? $action_error : null,
                                                'optional_date' => Yii::$app->formatter->asDate(
                                                    new \DateTime(), Yii::$app->params['actual_to_date_format_moment']
                                                )
                                            ]
                                        ),
                                        'toggleButton' => [
                                            'tag' => 'i',
                                            'label' => '',
                                            'class' => 'glyphicon glyphicon-plus cursor-pointer'
                                        ],
                                    ]
                                );
                                ?>
                            </div>
                            <div class="clearfix"></div>
                            <ul class="list-group actions-container"
                                id="actions-container-<?= $dateAsTimestamp ?>-<?= $t->id ?>">
                                <?php foreach ($actions_histories[$t->id] as $ah): ?>
                                    <?php
                                    $shouldMakeExpandableNote = $ah->action_class == 'Created'
                                        || $ah->action_class == 'Modified';
                                    $hasOptDt = (bool)$ah->optional_datetime;
                                    $user = $users_for_action_histories[$ah->id];
                                    ?>
                                    <li class="list-group-item">
                                        <div class="pull-left">
                                            <b><?= $user['display_name'] ?></b>:
                                            <?= \app\models\Action::convertClassNameToActionName($ah->action_class) ?>
                                            (<?= Yii::$app->formatter->asDate($ah->created_at, 'php:d.m.y H:i') ?>)
                                            <?php if ($hasOptDt): ?>
                                                , <?= Yii::t('app', 'Date') ?>: <?= Yii::$app->formatter->asDate($ah->optional_datetime, 'php:d.m.Y') ?>
                                            <?php endif; ?>
                                            <?php if (!$shouldMakeExpandableNote && $ah->note): ?>
                                                , <?= Yii::t('app', 'Note') ?>: <?= $ah->note ?>
                                            <?php endif; ?>
                                        </div>
                                        <?php if ($shouldMakeExpandableNote && $ah->note): ?>
                                            <div class="pull-right">
                                                <i data-block-id="action-note-<?= $dateAsTimestamp ?>-<?= $ah->id ?>"
                                                   class="expander glyphicon glyphicon-menu-down cursor-pointer"></i>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div id="action-note-<?= $dateAsTimestamp ?>-<?= $ah->id ?>"
                                                 class="well well-sm note-hidden">
                                                <p>
                                                    <?php if ($ah->optional_datetime): ?>
                                                        <?= Yii::t('app', 'Date') ?>: <?= Yii::$app->formatter->asDate($ah->optional_datetime, 'php:d.m.Y') ?>
                                                        <br/>
                                                    <?php endif; ?>
                                                    <?= nl2br($ah->note) ?>
                                                </p>
                                            </div>
                                        <?php else: ?>
                                            <div class="clearfix"></div>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<?php
Widget::$autoIdPrefix = $oldPrefix;
?>