<?php

use yii\helpers\Url;
use yii\base\Widget;
use kartik\popover\PopoverX;
use app\models\Task;

/* @var $this \yii\web\View */
/* @var array $tasks */
/* @var string $title */
/* @var string $date_attr */
/* @var array $task_groups_for_tasks */
/* @var array $task_types_for_tasks */
/* @var array $actions_histories */
/* @var array $users_for_tasks */
/* @var array $users_for_action_histories */

$oldPrefix = Widget::$autoIdPrefix;
Widget::$autoIdPrefix = '_tasks_list_w';
$this->registerJsFile('@web/js/expander.js', ['depends' => \yii\web\JqueryAsset::className()]);
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= $title ?></h3>
    </div>
    <div class="panel-body">
        <?php foreach ($tasks as $task): ?>
            <?php
            $task_type = $task_types_for_tasks[$task->id];
            $task_group = $task_groups_for_tasks[$task->id];
            $task_user = isset($users_for_tasks[$task->id]) ? $users_for_tasks[$task->id] : null;;
            ?>
            <div id="task-<?= $task->id ?>"
                 class="panel
                        panel-default
                        task-status-<?= Task::numericStatusToString($task->status) ?>
                        task-type-<?= $task_type['name'] ?><?php if ($task_user): ?>
                        task-performer-<?= $task_user['username'] ?><?php endif; ?><?php if ($task->for_all): ?>
                        for-all<?php endif; ?>">
                <div class="panel-heading task-group-<?= $task_group['name'] ?>">
                    <div class="pull-left">
                        <h3 class="panel-title">
                            <?= $task->name ?><?php if (isset($task->$date_attr)): ?>, <?= $task->$date_attr ?><?php endif; ?>
                            &nbsp;
                            (<?= $task_group['description'] ?> &rarr; <?= $task_type['description'] ?>)
                        </h3>
                    </div>
                    <div class="pull-right">
                        <i class="glyphicon glyphicon-menu-down cursor-pointer expander"
                           data-block-id="actions-container-<?= $task->id ?>"></i>
                        &nbsp;
                        <i class="glyphicon glyphicon-pencil modal-opener cursor-pointer"
                           data-task-id="<?= $task->id ?>"
                           data-modal-title="<?= $task->name; ?>"
                           data-modal-url="<?= Url::toRoute(['/task/view', 'id' => $task->id]) ?>"></i>&nbsp;
                        <?php
                        echo PopoverX::widget(
                            [
                                'header' => Yii::t('app', 'Manage'),
                                'class' => 'manage',
                                'placement' => PopoverX::ALIGN_LEFT,
                                'type' => PopoverX::TYPE_INFO,
                                'content' => $this->render('../task/_manage', ['model' => $task]),
                                'toggleButton' => [
                                    'tag' => 'i',
                                    'label' => '',
                                    'class' => 'glyphicon glyphicon-flag cursor-pointer'
                                ],
                            ]
                        );
                        ?>
                        &nbsp;
                        <?php
                        echo PopoverX::widget(
                            [
                                'header' => Yii::t('app', 'Action'),
                                'placement' => PopoverX::ALIGN_LEFT,
                                'type' => PopoverX::TYPE_SUCCESS,
                                'content' => $this->render(
                                    '../task/_add_new_action',
                                    [
                                        'model' => $task,
                                        'action_error' => isset($action_error) ? $action_error : null,
                                        'optional_date' => Yii::$app->formatter->asDate(
                                            new \DateTime(), Yii::$app->params['actual_to_date_format_moment']
                                        )
                                    ]
                                ),
                                'toggleButton' => [
                                    'tag' => 'i',
                                    'label' => '',
                                    'class' => 'glyphicon glyphicon-plus cursor-pointer'
                                ],
                            ]
                        );
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body actions-container" id="actions-container-<?= $task->id ?>">
                    <ul class="list-group">
                        <?php foreach ($actions_histories[$task->id] as $ah): ?>
                            <?php
                            $shouldMakeExpandableNote = $ah->action_class == 'Created'
                                || $ah->action_class == 'Modified';
                            $hasOptDt = (bool)$ah->optional_datetime;
                            $user = $users_for_action_histories[$ah->id];
                            ?>
                            <li class="list-group-item">
                                <b><?= $user['display_name'] ?></b>:
                                <?= \app\models\Action::convertClassNameToActionName($ah->action_class) ?>
                                (<?= Yii::$app->formatter->asDate($ah->created_at, 'php:d.m.Y H:i') ?>)
                                <?php if ($hasOptDt): ?>
                                    , <?= Yii::t('app', 'Date') ?>: <?= Yii::$app->formatter->asDate($ah->optional_datetime, 'php:d.m.Y') ?>
                                <?php endif; ?>
                                <?php if ($shouldMakeExpandableNote && $ah->note): ?>
                                    <div class="pull-right">
                                        <i data-block-id="action-note-<?= $ah->id ?>"
                                           class="expander glyphicon glyphicon-menu-down cursor-pointer"></i>
                                    </div>
                                    <div id="action-note-<?= $ah->id ?>" class="well well-sm note-hidden">
                                        <p>
                                            <?php if ($ah->optional_datetime): ?>
                                                <?= Yii::t('app', 'Date') ?>: <?= Yii::$app->formatter->asDate($ah->optional_datetime, 'php:d.m.Y') ?>
                                                <br/>
                                            <?php endif; ?>
                                            <?= nl2br($ah->note) ?>
                                        </p>
                                    </div>
                                <?php elseif ($ah->note): ?>
                                    , <?= Yii::t('app', 'Note') ?>: <?= $ah->note ?>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php
Widget::$autoIdPrefix = $oldPrefix;
?>

