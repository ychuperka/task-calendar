<?php

/* @var $tasks array */
/* @var $actions_histories */
/* @var $task_groups_for_tasks */
/* @var $task_types_for_tasks */
/* @var $users_for_tasks */
/* @var $users_for_action_histories */
$this->registerJsFile('@web/js/popovers-day-week.js', ['depends' => \yii\web\JqueryAsset::className()]);
?>
<br />
<?php
$count = 0;
foreach ($tasks as $group => $items) {
    $count += count($items);
}
?>
<?php if ($count == 0): ?>
    <h1><?= Yii::t('app', 'Empty search Result') ?></h1>
    <?php return; ?>
<?php endif; ?>
<?php 
$show_tasks_list = function ($view, $group, $title, $dateAttr) use (
    $tasks, $actions_histories,
    $task_groups_for_tasks, $task_types_for_tasks,
    $users_for_tasks, $users_for_action_histories
) {
    if (!isset($tasks[$group]) && count($tasks[$group]) == 0) {
        return null;
    } 
    return $view->render(
        '_tasks_list', 
        [
            'tasks' => $tasks[$group], 
            'title' => $title, 
            'date_attr' => $dateAttr,
            'actions_histories' => $actions_histories,
            'task_groups_for_tasks' => $task_groups_for_tasks,
            'task_types_for_tasks' => $task_types_for_tasks,
            'users_for_tasks' => $users_for_tasks,
            'users_for_action_histories' => $users_for_action_histories,
        ]
    );
} 
?>
<?php if (isset($tasks['actual']) && count($tasks['actual']) > 0): ?>
    <?= $show_tasks_list($this, 'actual', Yii::t('app', 'Actual Tasks'), 'actual_to') ?>
<?php endif; ?>
<?php if (isset($tasks['not_completed']) && count($tasks['not_completed']) > 0): ?>
    <?= $show_tasks_list($this, 'not_completed', Yii::t('app', 'Not Completed Tasks'), 'actual_to') ?>
<?php endif; ?>
<?php if (isset($tasks['postponed']) && count($tasks['postponed']) > 0): ?>
    <?= $show_tasks_list($this, 'postponed', Yii::t('app', 'Postponed Tasks'), 'actual_to') ?>
<?php endif; ?>
<?php if (isset($tasks['completed']) && count($tasks['completed']) > 0): ?>
    <?= $show_tasks_list($this, 'completed', Yii::t('app', 'Completed Tasks'), 'actual_to') ?>
<?php endif; ?>