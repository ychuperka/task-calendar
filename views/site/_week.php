<?php
/* @var $tasks array */
/* @var $actions_histories */
/* @var $task_groups_for_tasks */
/* @var $task_types_for_tasks */
/* @var $users_for_tasks */
/* @var $users_for_action_histories */
$this->registerJsFile('@web/js/popovers-day-week.js', ['depends' => \yii\web\JqueryAsset::className()]);
?>
<div class="row">
    <div class="col-xs-12" style="height:8px;"></div>
</div>
<nav>
    <ul id="week-pager" class="pager">
        <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> <?= Yii::t('app', 'Older') ?></a></li>
        <li class="next"><a href="#"><?= Yii::t('app', 'Newer') ?> <span aria-hidden="true">&rarr;</span></a></li>
    </ul>
</nav>
<?php if (count($tasks) == 0): ?>
    <h1><?= Yii::t('app', 'Empty search Result') ?></h1>
<?php else: ?>
    <?php
    $items_template = isset($actions_performed) && $actions_performed
        ? '_week_items_on_which_actions_performed' : '_week_common_items';
    $count = 0;
    ?>
    <?php foreach ($tasks as $date => $items): ?>
        <?php
        echo $this->render(
            $items_template,
            [
                'date' => $date,
                'items' => $items,
                'actions_histories' => $actions_histories,
                'task_groups_for_tasks' => $task_groups_for_tasks,
                'task_types_for_tasks' => $task_types_for_tasks,
                'users_for_tasks' => $users_for_tasks,
                'users_for_action_histories' => $users_for_action_histories,
            ]
        );
        ?>
        <?php ++$count; ?>
        <?php if ($count == 3): ?>
            <div class="clearfix"></div>
            <?php $count = 0; ?>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
<script type="text/javascript">
    (function ($) {

        // Check search executor avail
        if (searchExecutor === undefined || searchExecutor === null) {
            console.error("Week Pager: searchExecutor is not found or not initialized");
            return;
        }

        // Get controls
        var weekPager = $("ul#week-pager");
        var previousButton = weekPager.children("li.previous");
        var nextButton = weekPager.children("li.next");
        var dateSelectorInput = $("input#date-selector");

        /**
         *
         * @param direction
         */
        function forwardOrRewindADateWidget(direction) {

            if (direction === undefined) {
                direction = "forward";
            }

            var format = "<?= Yii::$app->params['actual_to_date_format_moment'] ?>";
            var momentDate = moment(dateSelectorInput.val(), format);
            var date = new Date(momentDate.toDate());
            var hoursToAdd = 0;
            if (direction === "forward") {
                hoursToAdd = 24 * 7;
            } else {
                hoursToAdd = -(24 * 7);
            }
            date.setHours(date.getHours() + hoursToAdd);

            momentDate = moment(date);
            dateSelectorInput.val(momentDate.format(format));
        }

        previousButton.click(function (event) {
            forwardOrRewindADateWidget("rewind");
            searchExecutor();
        });

        nextButton.click(function (event) {
            forwardOrRewindADateWidget("forward");
            searchExecutor();
        });


    })(jQuery);
</script>

