<?php

use yii\bootstrap\Tabs;
use yii\bootstrap\ButtonGroup;
use yii\bootstrap\Button;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use trntv\yii\datetime\DateTimeWidget;;

/* @var $this yii\web\View */
/* @var $tasks array */
/* @var $task_groups array */
/* @var $chat_begin_timestamp string */
/* @var $chat_messages_limit */
/* @var $actions_histories */
/* @var $task_groups_for_tasks */
/* @var $task_types_for_tasks */
/* @var $users_for_tasks */
/* @var $users_for_action_histories */

$this->title = Yii::t('app', 'Tasks');
$this->registerJsFile('@web/js/ajax-search.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerJsFile('@web/js/ajax-modals.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerJsFile('@web/js/chat.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerJs(
    '$("div#modal").on("hidden.bs.modal", function(event) {
        var theModal = $(this);
        theModal.find("div#modal-content").html("");
        searchExecutor();
    });'
);
$this->registerJs(
    '
    (function($) {
        moment.locale("ru");
    })(jQuery);
    '
);
$this->registerJs(
    '
    toastr.options.newestOnTop = false;
    toastr.options.closeButton = true;
    toastr.options.progressBar = true;
    toastr.options.preventDuplicates = true;
    toastr.options.positionClass = "toast-bottom-right";
    ',
    \yii\web\View::POS_END
);
$this->registerJs(
    '
    function showToastForChatMessage(sender, date, mainChat) {
        if (mainChat === undefined) {
            mainChat = true;
        }
        var message = mainChat ? "' . Yii::t('app', 'A new chat message') . '" : "' . Yii::t('app', 'New message in a task chat') . '";

        var mDt = moment(date, "YYYY-MM-DD HH:mm:ss");
        var sDt = mDt.format("HH:mm");
        var html = "<b>" + message + ", " + sDt
            + "</b><br /><p>' . Yii::t('app', 'Sender') . ': " + sender + "</p>";

        var oldTimeout = toastr.options.timeOut;
        var oldExtendedTimeout = toastr.options.extendedTimeOut;
        toastr.options.timeOut = 0;
        toastr.options.extendedTimeOut = 0;
        toastr.info(html);
        toastr.options.timeOut = oldTimeout;
        toastr.options.extendedTimeOut = oldExtendedTimeout;

        var sndNotification = document.getElementById("sound-notification");
        if (!!(sndNotification.canPlayType) && typeof(sndNotification.play) !== "undefined") {
            sndNotification.play();
        }
    }
    function showToastForAction(date, action_name, task_name) {

        var mDt = moment(date, "YYYY-MM-DD HH:mm:ss");
        var sDt = mDt.format("HH:mm");
        var html = "<b>' . Yii::t('app', 'An action performed') . '</b>: " + action_name
            + ", " + sDt + ", ' . Yii::t('app', 'Task') . ': " + task_name;

        var oldPositionClass = toastr.options.positionClass = "toast-bottom-right";
        toastr.options.positionClass = "toast-bottom-full-width";
        toastr.success(html);
        toastr.options.positionClass = oldPositionClass;
    }
    function showAlert(message) {
        toastr.warning(message);
    }
    ',
    \yii\web\View::POS_BEGIN
);
?>
<audio id="sound-notification" preload="auto">
    <source src="<?= Url::to('@web/sounds/notification.mp3') ?>">
    <source src="<?= Url::to('@web/sounds/notification.ogg') ?>">
</audio>
<div class="row">
    <div class="col-xs-2">
        <?php
        echo DateTimeWidget::widget(
            [
                'name' => 'date',
                'options' => [
                    'id' => 'date-selector',
                    'placeholder' => date(Yii::$app->params['actual_to_date_format_php']),
                ],
                'phpDatetimeFormat' => 'php:' . Yii::$app->params['actual_to_date_format_php'],
                'momentDatetimeFormat' => Yii::$app->params['actual_to_date_format_moment'],
                'clientOptions' => [
                    'allowInputToggle' => true
                ]
            ]
        ); 
        ?>
    </div>
    <div class="col-xs-2">
        <select id="task_groups" name="task_group" class="form-control">
            <option value="@"><?= Yii::t('app', 'All') ?></option>
            <?php foreach ($task_groups as $tg): ?>
                <option value="<?= $tg->id ?>"><?= $tg->description ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-xs-6">
        <div class="input-group">
            <?php
            echo Html::input(
                'text', 'name', '',
                [
                    'id' => 'search-input',
                    'class' => 'form-control',
                    'placeholder' => Yii::t('app', 'Search for...')
                ]
            );
            ?>
            <span class="input-group-btn">
                <?php 
                echo Button::widget(
                    [
                        'label' => Yii::t('app', 'Search!'),
                        'options' => [
                            'id' => 'search-button',
                            'class' => 'btn-default'
                        ],
                    ]
                ); 
                ?>
            </span>
        </div>
    </div>
    <div class="col-xs-2">
        <?php
        echo ButtonGroup::widget(
            [
                'buttons' => [
                    [
                        'label' => Yii::t('app', 'Add New'),
                        'options' => [
                            'class' => 'btn-success modal-opener',
                            'data-modal-title' => Yii::t('app', 'Create new task'),
                            'data-modal-url' => Url::toRoute('/task/create'),
                        ],
                    ],
                ],
            ]
        );
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12" style="height:8px;"></div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?php
        echo $this->render(
            '_chat', [
                'chat_begin_timestamp' => $chat_begin_timestamp,
                'chat_messages_limit' => $chat_messages_limit,
                'chat_id' => 'main'
            ]
        )
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?php
        echo Tabs::widget(
            [
                'items' => [
                    [
                        'label' => Yii::t('app', 'Day'),
                        'content' => $this->render(
                            '_day', [
                                'tasks' => $tasks,
                                'actions_histories' => $actions_histories,
                                'task_groups_for_tasks' => $task_groups_for_tasks,
                                'task_types_for_tasks' => $task_types_for_tasks,
                                'users_for_tasks' => $users_for_tasks,
                                'users_for_action_histories' => $users_for_action_histories
                            ]
                        ),
                        'headerOptions' => ['class' => 'tab-header', 'data-url' => Url::toRoute('/site/day')],
                        'options' => ['id' => 'tab-content-day'],
                        'active' => true,
                    ],
                    [
                        'label' => Yii::t('app', 'Week'),
                        'headerOptions' => ['class' => 'tab-header', 'data-url' => Url::toRoute('/site/week')],
                        'options' => ['id' => 'tab-content-week'],
                    ],
                    [
                        'label' => Yii::t('app', 'Acts.Performed'),
                        'headerOptions' => ['class' => 'tab-header', 'data-url' => Url::toRoute('/site/week-actions-performed')],
                        'options' => ['id' => 'tab-content-week-actions-performed']
                    ]
                ]
            ]
        );
        ?>
    </div>
</div>
<?php
Modal::begin(
    [
        'id' => 'modal',
        'size' => Modal::SIZE_LARGE,
        'header' => '<span id="modal-header-title">Modal Window</span>',
        'clientOptions' => [
            'backdrop' => true,
        ]
    ]
);
?>
<div id="modal-content"></div>
<?php Modal::end(); ?>
