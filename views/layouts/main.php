<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use trntv\yii\datetime\assets\MomentAsset;
use lavrentiev\widgets\toastr\assets\ToastrAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
MomentAsset::register($this);
ToastrAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => Yii::$app->params['app_title'],
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);


            $items = [];
            $items[] = ['label' => Yii::t('app', 'Home'), 'url' => ['/site/index']];
            if (Yii::$app->user->can('view_user')) {
                $items[] = ['label' => Yii::t('app', 'Users'), 'url' => ['/user']];
            }
            $items[] = Yii::$app->user->isGuest
                    ? ['label' => Yii::t('app', 'Login'), 'url' => ['/site/login']]
                    : [
                        'label' => Yii::t('app', 'Logout') . ' (' . Yii::$app->user->identity->display_name . ')',
                        'url' => ['/site/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ];
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $items,
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; <?= Yii::$app->params['company_name'] ?> <?= date('Y') ?></p>
            <p class="pull-right">
                <?= Yii::t('app', 'Developer') ?> <a href="http://www.ychuperka.com" target="_blank" rel="external">Yegor Chuperka</a><br />
                <?= Yii::powered() ?>
            </p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
