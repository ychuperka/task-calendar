<?php

return [
    'app_title' => 'Task Calendar',
    'admin_email' => 'admin@example.com',
    'company_name' => 'My Company',
    'completed_tasks_limit' => 10,
    'actual_to_date_format_php' => 'd.m.Y',
    'actual_to_date_format_moment' => 'DD.MM.YYYY',
    'chat_update_interval_ms' => 5000,
    'payer_user_login' => 'maxim',
    'action_other_user_login' => 'admin',
    'chat_load_last_messages_count' => 5,
];
