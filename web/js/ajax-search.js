var searchExecutor = null;

(function($) {

    // Get search controls
    var searchButton = $("button#search-button");
    var searchInput = $("input#search-input");
    var taskGroupsSelect = $("select#task_groups")
    var dateSelectorInput = $("input#date-selector");
    var tabs = $("li.tab-header");

    /**
     * The function toggles controls (enabled/disabled)
     */
    var toggleControls = function() {
        var controls = [
            searchButton, searchInput, dateSelectorInput, taskGroupsSelect
        ];
        for(var i = 0; i < controls.length; ++i) {
            controls[i].prop("disabled", !controls[i].attr("disabled"));
        }
    };

    /**
     * The function executes a search
     *
     * @param activeTab Used when search executes from a tab click handler
     */
    searchExecutor = function (activeTab, callback) {

        toggleControls();

        if (activeTab === undefined ) {
            activeTab = $("li.tab-header.active");
        }

        var activeTabAnchorHref = activeTab.children("a").attr("href");
        if (activeTabAnchorHref === "#tab-content-week"
            || activeTabAnchorHref === "#tab-content-week-actions-performed") {
            var momentDate = dateSelectorInput.val().length > 0
                ? moment(dateSelectorInput.val(), "DD.MM.YYYY")
                : moment(new Date());
            var firstWeekDayMomentDate = momentDate.startOf("week");
            dateSelectorInput.val(firstWeekDayMomentDate.format("DD.MM.YYYY"));
        }

        var activeTabContentsBlockId = activeTab.children("a").attr("href").substr(1);
        var activeTabContentsBlock = $("div#" + activeTabContentsBlockId);

        // Prepare params for the request
        var date = dateSelectorInput.val();
        var name = searchInput.val();
        var taskGroupId = taskGroupsSelect.val();

        var params = {};
        if (date.length > 0) {
            params.date = date;
        }
        if (name.length > 0) {
            params.name = name;
        }
        if (taskGroupId.length > 0 && taskGroupId != "@") {
            params.task_group_id = taskGroupId;
        }

        // Show loader
        activeTabContentsBlock.html("<div class=\"loader-container\"><img src=\"/img/loader.gif\" alt=\"Loading...\" /></div>");

        // Load search result
        var handlerPath = activeTab.data("url");
        activeTabContentsBlock.load(handlerPath, params, function() {
            toggleControls();

            if (updateModalOpenerEvents !== undefined) {
                updateModalOpenerEvents();
            }

            if (updatePopoversEvents !== undefined) {
                updatePopoversEvents();
            }

            if (updateExpanders !== undefined) {
                updateExpanders();
            }

            /*
            Update PopoverX handlers (bad design boy... bad)
             */
            $("[data-toggle='popover-x']").on('click', function (e) {
                var $this = $(this), href = $this.attr('href'),
                    $dialog = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))), //strip for ie7
                    option = $dialog.data('popover-x') ? 'toggle' : $.extend({remote: !/#/.test(href) && href},
                        $dialog.data(), $this.data());
                e.preventDefault();
                $dialog.trigger('click.target.popoverX');
                if (option !== 'toggle') {
                    option.$target = $this;
                    $dialog
                        .popoverX(option)
                        .popoverX('show')
                        .on('hide', function () {
                            $this.focus();
                        });
                }
                else {
                    $dialog
                        .popoverX(option)
                        .on('hide', function () {
                            $this.focus();
                        });
                }
            });

            $('[data-toggle="popover-x"]').on('keyup', function (e) {
                var $this = $(this), href = $this.attr('href'),
                    $dialog = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))); //strip for ie7
                if ($dialog && e.which === 27) {
                    $dialog.trigger('keyup.target.popoverX');
                    $dialog.popoverX('hide');
                }
            });

            // Call callback
            if (callback !== undefined) {
                callback();
            }
        });
    };

    // Execute search if search button is clicked
    searchButton.click(function(event) {
        if (searchInput.val().length > 0) {
            var anchor = $("a[href=\"#tab-content-day\"]");
            anchor.click();
            searchExecutor(anchor.parent());
        } else {
            searchExecutor();
        }
    });

    // Execute search if date is changes
    dateSelectorInput.focusout(function(event) {
        searchExecutor();
    });

    // Execute search if task group changes
    taskGroupsSelect.change(function(event) {
        searchExecutor();
    });

    // Execute search if tab is clicked
    tabs.click(function(event) {

        var theTabAnchor = $(this).children("a");
        if (theTabAnchor.attr("href") == "#tab-content-day") {
            dateSelectorInput.val("");
        }

        searchExecutor($(this));
    });


})(jQuery);