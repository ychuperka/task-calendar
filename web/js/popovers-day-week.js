function updatePopoversEvents() {

    /**
     *
     * @param $form
     * @param successMessage
     * @param failedMessage
     */
    function handlerLogic($form, successMessage, failedMessage) {
        var dataToSend = $form.serializeArray();
        dataToSend.push({name: "json", value: true});
        $.ajax({
            url: $form.attr("action"),
            method: $form.attr("method"),
            dataType: "json",
            data: dataToSend,
            success: function (response) {

                if (!response.hasOwnProperty("success") || !response.success) {
                    console.error(failedMessage);
                    if (response.hasOwnProperty("message")) {
                        if (showAlert !== undefined) {
                            showAlert(response.message);
                        } else {
                            alert(response.message);
                        }
                    }
                    return;
                }
                console.log(successMessage);
                if (showToastForAction !== undefined) {
                    showToastForAction(response.action_history_created_at, response.action_name, response.task_name);
                }

                $("div.popover").popoverX("hide");

                if (searchExecutor !== undefined) {
                    searchExecutor();
                }
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    }

    /*
    Manage
     */
    $("div.popover form.form-set-status").submit(function(event) {
        event.preventDefault();
        handlerLogic($(this), "Set status is succeed.", "Set status is failed!");
        return false;
    });

    /*
    Do action
     */
    $("div.popover form.form-action-do").submit(function (event) {
        event.preventDefault();
        handlerLogic($(this), "Do Action success.", "Do Action failed!");
        return false;
    });

}
updatePopoversEvents();