function Chat(user_id, chat_id, task_id) {

    this.showNotifications = false;
    this.userId = user_id;
    var thisInstance = this;

    var chatBlock = $(chat_id);
    if (chatBlock.length == 0) {
        return;
    }

    var expander = chatBlock.find("i.chat-expander");
    var chatContainer = chatBlock.find("div.chat-container");
    var chatContentsContainer = chatContainer.find("div.chat-contents-container");
    var form = chatContainer.find("form.form-chat");
    var anchorPrevious = chatContainer.find("a.previous");
    var userId = chatContentsContainer.data("user-id");
    var initialState = true;

    // If task_id is defined the add it as a hidden field to
    // the send form
    if (task_id !== undefined) {
        form.append("<input type=\"hidden\" name=\"task_id\" value=\"" + task_id + "\" />");
    }

    // Update messages every "n" seconds
    var lastUpdateTimestamp = chatContentsContainer.data("timestamp");
    var _shouldExecuteNextUpdate = true;
    var _updateMessagesTimeout = null;

    /**
     * Builds a message view using message`s data
     *
     * @param message
     * @returns {*|HTMLElement}
     */
    function buildAMessageView(message) {
        var messageBlock = $("<div class=\"chat-message-container\" data-id=\"" + message.id + "\"></div>");
        var attrClass = "message-header";
        if (message.user_id == userId) {
            attrClass += " message-header-own";
        }
        messageBlock.append("<span class=\"" + attrClass + "\">[" + message.created_at + "] " + message.username + "</span>");
        messageBlock.append("<p>" + message.message + "</p>");
        return messageBlock;
    }

    /**
     * Update messages
     * @param successCallback
     */
    function updateMessages(successCallback) {

        var dataToSend = {
            from_timestamp: lastUpdateTimestamp
        };
        if (task_id !== undefined) {
            dataToSend["task_id"] = task_id;
        }

        $.ajax({
            url: chatContentsContainer.data("url"),
            method: "GET",
            dataType: "json",
            data: dataToSend,
            success: function (response) {

                if (!document.body.contains(chatContentsContainer[0])) {
                    return;
                }

                // Put messages to a messages container
                for (var i = 0; i < response.messages.length; ++i) {
                    var message = response.messages[i];
                    if (chatContentsContainer.find("div.chat-message-container[data-id=" + message.id + "]").length > 0) {
                        continue;
                    }
                    chatContentsContainer.append(
                        buildAMessageView(message)
                    );
                    if (thisInstance.showNotifications
                        && thisInstance.userId != message.user_id
                        && showToastForChatMessage !== undefined) {
                        showToastForChatMessage(message.username, message.created_at, task_id === undefined);
                    }
                }

                if (initialState) {
                    thisInstance.turnOnNotifications();
                    initialState = false;
                }

                // Update last read timestamp
                lastUpdateTimestamp = response.timestamp;

                if (successCallback !== undefined) {
                    successCallback();
                }

                // Execute the code again after n seconds
                if (_shouldExecuteNextUpdate) {
                    _updateMessagesTimeout = setTimeout(updateMessages, 5000);
                }

            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    }

    updateMessages();

    /**
     * Turn off messages updates
     */
    function turnOffUpdates() {
        _shouldExecuteNextUpdate = false;
        clearTimeout(_updateMessagesTimeout);
    }

    /**
     * Turn on messages updates
     */
    function turnOnUpdates() {
        _shouldExecuteNextUpdate = true;
    }

    // Toggle chat if control clicked
    expander.click(function (event) {
        if (expander.hasClass("glyphicon-menu-down")) {

            chatContainer.slideDown("fast");
            expander.removeClass("glyphicon-menu-down");
            expander.addClass("glyphicon-menu-up");

            // Scroll the chat to bottom
            chatContentsContainer.scrollTop(chatContentsContainer[0].scrollHeight);

        } else if (expander.hasClass("glyphicon-menu-up")) {

            chatContainer.slideUp("fast");
            expander.removeClass("glyphicon-menu-up");
            expander.addClass("glyphicon-menu-down");

        }
    });

    // On form submit send a message
    form.submit(function (event) {
        event.preventDefault();
        turnOffUpdates();

        var messageInput = form.find("input#chat-message");
        if (messageInput.val().length == 0) {
            return false;
        }

        var submitButton = form.find("button[type=submit]");

        function toggleControls() {
            messageInput.prop("disabled", !messageInput.attr("disabled"));
            submitButton.prop("disabled", !submitButton.attr("disabled"));
        }

        var dataToSend = form.serialize();
        toggleControls();

        $.ajax({
            url: form.attr("action"),
            method: form.attr("method"),
            dataType: "json",
            data: dataToSend,
            success: function (response) {
                if (!response.hasOwnProperty("success") || !response.success) {
                    var message = response.hasOwnProperty("message") ? ", server response:\n" + response.message : "";
                    alert("Can not send a message" + message);
                    return;
                }

                turnOnUpdates();
                updateMessages(function () {
                    messageInput.val("");
                    toggleControls();

                    // Scroll the chat to bottom
                    chatContentsContainer.scrollTop(chatContentsContainer[0].scrollHeight);
                });
            },
            error: function (response) {
                alert(response.responseText);
            }
        });

        return false;
    });

    // On clicked link "Load previous messages"
    function anchorPreviousClickHandler(event) {
        event.preventDefault();

        var firstMessage = chatContainer.find("div.chat-message-container:first");
        if (firstMessage.length == 0) {
            return false;
        }
        var firstMessageId = firstMessage.data("id");

        // Show loader and hide anchor
        anchorPrevious.parent().find("img").toggle();
        anchorPrevious.toggle();

        var dataToSend = {
            first_message_id: firstMessageId
        };
        if (task_id !== undefined) {
            dataToSend["task_id"] = task_id;
        }

        $.ajax({
            url: anchorPrevious.attr("href"),
            method: "GET",
            dataType: "json",
            data: dataToSend,
            success: function (response) {
                if (!response.hasOwnProperty("messages")) {
                    return;
                }

                for (var i = response.messages.length - 1; i >= 0; --i) {
                    chatContentsContainer.prepend(
                        buildAMessageView(response.messages[i])
                    );
                }

                var anchorParent = anchorPrevious.parent();
                anchorParent.remove();

                // If has more messages then move anchor parent to the top,
                // hide loader and show the anchor
                if (response.hasOwnProperty("has_more") && response.has_more) {
                    chatContentsContainer.prepend(anchorParent);
                    anchorParent.find("img").toggle();
                    anchorPrevious.toggle();
                    anchorParent.click(anchorPreviousClickHandler);
                }
            },
            error: function (response) {
                alert(response.responseText);
            }
        });

        return false;
    }

    anchorPrevious.click(anchorPreviousClickHandler);
}

Chat.prototype.turnOffNotifications = function () {
    this.showNotifications = false;
};

Chat.prototype.turnOnNotifications = function () {
    this.showNotifications = true;
};