function updateModalOpenerEvents() {
    var modalWindowBlock = $("div#modal");
    var modalWindowBlockTitleSpan = modalWindowBlock.find("span#modal-header-title");
    var modalWindowBlockContentBlock = modalWindowBlock.find("div#modal-content");

    $(".modal-opener").off("click").on("click", function(event) {
        var theButton = $(this);
        modalWindowBlockTitleSpan.html(theButton.data("modal-title"));
        modalWindowBlockContentBlock.html("<div class=\"loader-container\"><img src=\"/img/loader.gif\" alt=\"Loading...\" /></div>");
        modalWindowBlock.modal();
        modalWindowBlockContentBlock.load(theButton.data("modal-url"));
    });
}
updateModalOpenerEvents();