function updateExpanders() {
    $("i.expander").click(function (event) {
        var expander = $(this);
        var block_id = expander.data("block-id");

        if (expander.hasClass("glyphicon-menu-down")) {
            expander.removeClass("glyphicon-menu-down");
            expander.addClass("glyphicon-menu-up");
            $("div#" + block_id + ", ul#" + block_id).slideDown("fast");
        } else if (expander.hasClass("glyphicon-menu-up")) {
            expander.removeClass("glyphicon-menu-up");
            expander.addClass("glyphicon-menu-down");
            $("div#" + block_id + ", ul#" + block_id).slideUp("fast");
        }

    });
}
updateExpanders();