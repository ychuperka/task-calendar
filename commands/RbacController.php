<?php

namespace app\commands;

use app\rbac\OwnerRule;
use app\rbac\ForAllRule;
use Yii;
use yii\helpers\Console;

class RbacController extends \yii\console\Controller
{
    public function actionInit()
    {
        $this->stdout('Initializing roles and permissions...' . PHP_EOL, Console::FG_GREEN);

        $authManager = Yii::$app->authManager;

        /*
         * Remove ALL!
         */
        $authManager->removeAll();

        /*
         * Rules
         */
        // A owner rule (for actions that owner can do with his items)
        $ownerRule = new OwnerRule();
        $this->addRule($authManager, $ownerRule);

        // For all rule (if need to show task for all users)
        $forAllRule = new ForAllRule();
        $this->addRule($authManager, $forAllRule);

        /*
         * Permissions
         */
        // Permission to view task
        $viewTask = $authManager->createPermission('view_task');
        $viewTask->description = Yii::t('app', 'View Task');

        // Permission to view own task
        $viewOwnTask = $authManager->createPermission('view_own_task');
        $viewOwnTask->description = Yii::t('app', 'View own task');
        $this->addRuleToPermission($ownerRule, $viewOwnTask);

        // Permission to view task for all
        $viewTaskForAll = $authManager->createPermission('view_task_for_all');
        $viewTaskForAll->description = Yii::t('app', 'View task for All');
        $this->addRuleToPermission($forAllRule, $viewTaskForAll);

        // Permission to create task
        $createTask = $authManager->createPermission('create_task');
        $createTask->description = Yii::t('app', 'Create Task');

        // Permission to delete task
        $deleteTask = $authManager->createPermission('delete_task');
        $deleteTask->description = Yii::t('app', 'Delete Task');

        // Permission to delete own task
        $deleteOwnTask = $authManager->createPermission('delete_own_task');
        $deleteOwnTask->description = Yii::t('app', 'Delete own Task');
        $this->addRuleToPermission($ownerRule, $deleteOwnTask);

        // Permission to create task double
        $createTaskDouble = $authManager->createPermission('create_task_double');
        $createTaskDouble->description = Yii::t('app', 'Create task double');

        // Permission to create own task double
        $createOwnTaskDouble = $authManager->createPermission('create_own_task_double');
        $createOwnTaskDouble->description = Yii::t('app', 'Create Own task Double');
        $this->addRuleToPermission($ownerRule, $createOwnTaskDouble);

        // Permission to create double of task which for all
        $createTaskWhichForAllDouble = $authManager->createPermission('create_task_which_for_all_double');
        $createTaskWhichForAllDouble->description = Yii::t('app', 'Create task double from task which for all');
        $this->addRuleToPermission($forAllRule, $createTaskWhichForAllDouble);

        // Permission to update task
        $updateTask = $authManager->createPermission('update_task');
        $updateTask->description = Yii::t('app', 'Update Task');

        // Permission to update own task
        $updateOwnTask = $authManager->createPermission('update_own_task');
        $updateOwnTask->description = Yii::t('app', 'Update Own Task');
        $this->addRuleToPermission($ownerRule, $updateOwnTask);

        // Permission to update task which for all
        $updateTaskWhichForAll = $authManager->createPermission('update_task_which_for_all');
        $updateTaskWhichForAll->description = Yii::t('app', 'Update task which for All');
        $this->addRuleToPermission($forAllRule, $updateTaskWhichForAll);

        // Permission to change task user
        $changeTaskUser = $authManager->createPermission('change_task_user');
        $changeTaskUser->description = Yii::t('app', 'Change tak User');

        // Permission to do action
        $doAction = $authManager->createPermission('do_action');
        $doAction->description = Yii::t('app', 'Do Action');

        // Permission to do action with own task
        $doActionWithOwnTask = $authManager->createPermission('do_action_with_own_task');
        $doActionWithOwnTask->description = Yii::t('app', 'Do Action with own Task');
        $this->addRuleToPermission($ownerRule, $doActionWithOwnTask);

        // Permission to do action with task which for all
        $doActionWithTaskWhichForAll = $authManager->createPermission('do_action_with_task_which_for_all');
        $doActionWithTaskWhichForAll->description = Yii::t('app', 'Do Action with task which for All');
        $this->addRuleToPermission($forAllRule, $doActionWithTaskWhichForAll);

        // Permission to send message to main chat
        $sendMessageToMainChat = $authManager->createPermission('send_message_to_main_chat');
        $sendMessageToMainChat->description = Yii::t('app', 'Send Message to Main Chat');

        // Permission to read messages from main chat
        $readMessagesFromMainChat = $authManager->createPermission('read_messages_from_main_chat');
        $readMessagesFromMainChat->description = Yii::t('app', 'Read messages from Main Chat');

        // Permission to send message to a task
        $sendMessageToTask = $authManager->createPermission('send_message_to_task');
        $sendMessageToTask->description = Yii::t('app', 'Send Message to Task');

        // Permission to send message to own task
        $sendMessageToOwnTask = $authManager->createPermission('send_message_to_own_task');
        $sendMessageToOwnTask->description = Yii::t('app', 'Send Message to own Task');
        $this->addRuleToPermission($ownerRule, $sendMessageToOwnTask);

        // Permission to send message to task which for all
        $sendMessageToTaskWhichForAll = $authManager->createPermission('send_message_to_task_which_for_all');
        $sendMessageToTaskWhichForAll->description = Yii::t('app', 'Send Message to task which for all');
        $this->addRuleToPermission($forAllRule, $sendMessageToTaskWhichForAll);

        // Permission to read messages from task
        $readMessagesFromTask = $authManager->createPermission('read_messages_from_task');
        $readMessagesFromTask->description = Yii::t('app', 'Read messages from task');

        // Permission to read messages from own task
        $readMessagesFromOwnTask = $authManager->createPermission('read_messages_from_own_task');
        $readMessagesFromOwnTask->description = Yii::t('app', 'Read messages from own task');
        $this->addRuleToPermission($ownerRule, $readMessagesFromOwnTask);

        // Permission to read message from task which for all
        $readMessagesFromTaskWhichForAll = $authManager->createPermission('read_messages_from_task_which_for_all');
        $readMessagesFromTaskWhichForAll->description = Yii::t('app', 'Read messages from task which for all');
        $this->addRuleToPermission($forAllRule, $readMessagesFromTaskWhichForAll);

        // Permission to view user
        $viewUser = $authManager->createPermission('view_user');
        $viewUser->description = Yii::t('app', 'View User');

        // Permission to create user
        $createUser = $authManager->createPermission('create_user');
        $createUser->description = Yii::t('app', 'Create User');

        // Permission to update user
        $updateUser = $authManager->createPermission('update_user');
        $updateUser->description = Yii::t('app', 'Update User');

        // Permission to delete user
        $deleteUser = $authManager->createPermission('delete_user');
        $deleteUser->description = Yii::t('app', 'Delete User');

        // Add permissions to the auth manager
        $permissions = [
            $viewTask, $viewOwnTask, $viewTaskForAll,
            $createTask, $createTaskDouble, $createOwnTaskDouble, $createTaskWhichForAllDouble,
            $updateTask, $updateOwnTask, $updateTaskWhichForAll,
            $deleteTask, $deleteOwnTask,
            $changeTaskUser,

            $doAction, $doActionWithOwnTask, $doActionWithTaskWhichForAll,

            $sendMessageToTask, $sendMessageToOwnTask, $sendMessageToTaskWhichForAll,
            $readMessagesFromTask, $readMessagesFromOwnTask, $readMessagesFromTaskWhichForAll,

            $viewUser, $createUser, $updateUser, $deleteUser,

            $sendMessageToMainChat, $readMessagesFromMainChat,

        ];
        foreach ($permissions as $item) {
            $this->addPermission($authManager, $item);
        }

        // Assign permissions with creator rule to base permissions
        $relations = [
            [$viewTask, $viewOwnTask],
            [$viewTask, $viewTaskForAll],
            [$createTaskDouble, $createOwnTaskDouble],
            [$createTaskDouble, $createTaskWhichForAllDouble],
            [$updateTask, $updateOwnTask],
            [$updateTask, $updateTaskWhichForAll],
            [$deleteTask, $deleteOwnTask],
            [$sendMessageToTask, $sendMessageToOwnTask],
            [$sendMessageToTask, $sendMessageToTaskWhichForAll],
            [$readMessagesFromTask, $readMessagesFromOwnTask],
            [$readMessagesFromTask, $readMessagesFromTaskWhichForAll],
            [$doAction, $doActionWithOwnTask],
            [$doAction, $doActionWithTaskWhichForAll],
        ];
        foreach ($relations as $item) {
            $this->addSubPermission($authManager, $item[1], $item[0]);
        }

        /*
         * Roles
         */
        // User (common users)
        $user = $authManager->createRole('user');
        $this->addRole($authManager, $user);
        $permissions = [

            $createTask, $createOwnTaskDouble, $createTaskWhichForAllDouble,
            $viewOwnTask, $viewTaskForAll,
            $updateOwnTask, $updateTaskWhichForAll,
            $changeTaskUser,

            $doActionWithOwnTask, $doActionWithTaskWhichForAll,

            $sendMessageToOwnTask, $sendMessageToTaskWhichForAll,
            $readMessagesFromOwnTask, $readMessagesFromTaskWhichForAll,

            $sendMessageToMainChat, $readMessagesFromMainChat,
        ];
        foreach ($permissions as $item) {
            $this->addPermissionToRole($authManager, $user, $item);
        }

        // Admin (super user, extends a user role)
        $admin = $authManager->createRole('admin');
        $this->addRole($authManager, $admin);
        $this->addSubRole($authManager, $admin, $user);
        $permissions = [
            $viewTask, $createTask, $updateTask, $deleteTask,

            $viewUser, $createUser, $updateUser, $deleteUser,

            $sendMessageToTask, $readMessagesFromTask,

            $doAction, $createTaskDouble,
        ];
        foreach ($permissions as $item) {
            $this->addPermissionToRole($authManager, $admin, $item);
        }

        $this->stdout(Yii::t('app', 'Done.') . PHP_EOL, Console::FG_GREEN);

        return 0;
    }

    /**
     * Adds a permission to a role (using names)
     *
     * @param string $permissionName A permission name
     * @param string $roleName A role name
     */
    public function actionAddPermissionToRole($permissionName, $roleName)
    {
        $authManager = Yii::$app->authManager;

        $permission = $authManager->getPermission($permissionName);
        if (!$permission) {
            $this->stderr("The permission \"$permissionName\" not found" . PHP_EOL, Console::FG_RED);
            return;
        }
        $role = $authManager->getRole($roleName);
        if (!$role) {
            $this->stderr("The role \"$roleName\" not found" . PHP_EOL, Console::FG_RED);
            return;
        }

        $this->addPermissionToRole($authManager, $role, $permission);
    }

    /**
     * @param \yii\rbac\ManagerInterface $authManager
     * @param \yii\rbac\Permission $permission
     */
    protected function addPermission(\yii\rbac\ManagerInterface $authManager, \yii\rbac\Permission $permission)
    {
        $authManager->add($permission);
        $this->stdout(Yii::t('app', 'Permission added, name') . ': ' . $permission->name . PHP_EOL, Console::FG_YELLOW);
    }

    /**
     * @param \yii\rbac\ManagerInterface $authManager
     * @param \yii\rbac\Permission $parent
     * @param \yii\rbac\Permission $child
     */
    protected function addSubPermission(\yii\rbac\ManagerInterface $authManager, \yii\rbac\Permission $parent, \yii\rbac\Permission $child)
    {
        $authManager->addChild($parent, $child);
        $this->stdout(
            Yii::t('app', 'Permission assigned to another permission, parent') . ': ' . $parent->name
                . ', ' . Yii::t('app', 'child') . ': ' . $child->name . PHP_EOL,
            Console::FG_YELLOW
        );
    }

    /**
     * @param \yii\rbac\ManagerInterface $authManager
     * @param \yii\rbac\Role $role
     */
    protected function addRole(\yii\rbac\ManagerInterface $authManager, \yii\rbac\Role $role)
    {
        $authManager->add($role);
        $this->stdout(Yii::t('app', 'Role added, name') . ': ' . $role->name . PHP_EOL, Console::FG_YELLOW);
    }

    /**
     * @param \yii\rbac\ManagerInterface $authManager
     * @param \yii\rbac\Role $parent
     * @param \yii\rbac\Role $child
     */
    protected function addSubRole(\yii\rbac\ManagerInterface $authManager, \yii\rbac\Role $parent, \yii\rbac\Role $child)
    {
        $authManager->addChild($parent, $child);
        $this->stdout(
            Yii::t('app', 'Role assigned to another role, parent') . ': ' . $parent->name . ', ' . Yii::t('app', 'child')
                . ': ' . $child->name . PHP_EOL,
            Console::FG_YELLOW
        );
    }

    /**
     * @param \yii\rbac\ManagerInterface $authManager
     * @param \yii\rbac\Role $role
     * @param \yii\rbac\Permission $permission
     */
    protected function addPermissionToRole(\yii\rbac\ManagerInterface $authManager, \yii\rbac\Role $role, \yii\rbac\Permission $permission)
    {
        if ($authManager->hasChild($role, $permission)) {
            $this->stderr(
                Yii::t('app', 'The permission already assigned to the role') . PHP_EOL,
                Console::FG_RED
            );
            return;
        }
        $authManager->addChild($role, $permission);
        $this->stdout(
            Yii::t('app', 'Permission assigned to role') . ': ' . $role->name . ', ' . Yii::t('app', 'permission') . ': '
                . $permission->name . PHP_EOL,
            Console::FG_YELLOW
        );
    }

    /**
     * @param \yii\rbac\ManagerInterface $authManager
     * @param \yii\rbac\Rule $rule
     */
    protected function addRule(\yii\rbac\ManagerInterface $authManager, \yii\rbac\Rule $rule)
    {
        $authManager->add($rule);
        $this->stdout(
            Yii::t('app', 'Rule added, name') . ': ' . $rule->name . PHP_EOL,
            Console::FG_YELLOW
        );
    }

    /**
     * @param \yii\rbac\Rule $rule
     * @param \yii\rbac\Permission $permission
     */
    protected function addRuleToPermission(\yii\rbac\Rule $rule, \yii\rbac\Permission $permission)
    {
        $permission->ruleName = $rule->name;
        $this->stdout(
            Yii::t('app', 'Rule assigned to permission') . ': ' . $permission->name . ', '
                . Yii::t('app', 'rule') . ': ' . $rule->name . PHP_EOL,
            Console::FG_YELLOW
        );
    }

}
