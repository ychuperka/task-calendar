<?php

namespace app\commands;

use Yii;
use app\models\User;
use yii\helpers\Console;

class UserController extends \yii\console\Controller
{
    /**
     * @param string $username Username
     * @param string $password Password
     * @param string $passwordAgain Password again
     * @param string $role Role
     * @return int
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate($username, $password, $passwordAgain, $role = 'user')
    {
        $user = new User();
        $user->username = $username;
        $user->display_name = ucfirst($username);
        $user->password = $password;
        $user->check = $passwordAgain;
        $user->role = $role;
        if (!$user->save()) {
            $errors = $user->getErrors();
            $message = '';
            foreach ($errors as $field => $error_text) {
                $error_text = implode(', ', $error_text);
                $message .= "$field -> $error_text\n";
            }
            $this->stderr($message, Console::FG_RED);
            return 1;
        }

        $this->stdout(Yii::t('app', 'Success') . '! ' . Yii::t('app', 'User ID') . ': ' . $user->id . PHP_EOL, Console::FG_GREEN);

        return 0;
    }

    public function actionDelete()
    {
    }

    public function actionUpdate()
    {
    }

}
