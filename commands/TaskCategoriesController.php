<?php

namespace app\commands;

use app\models\TaskGroup;
use app\models\TaskType;
use app\models\TaskTypeToTaskGroup;
use Yii;
use yii\helpers\Console;

class TaskCategoriesController extends \yii\console\Controller
{
    public function actionCreate()
    {

        $this->stdout(
            Yii::t('app', 'Initializing task categories') . '...' . PHP_EOL, Console::FG_GREEN
        );

        TaskTypeToTaskGroup::deleteAll();
        TaskGroup::deleteAll();
        TaskType::deleteAll();

        /*
         * Create task groups
         */
        $groups = [
            'finance' => Yii::t('app', 'Finance'),
            'communication' => Yii::t('app', 'Communication'),
            'urgent' => Yii::t('app', 'Urgent'),
            'work' => Yii::t('app', 'Work'),
            'check' => Yii::t('app', 'Check'),
            'problem' => Yii::t('app', 'Problem'),
        ];
        $groupsIds = [];
        foreach ($groups as $name => $description) {
            $id = $this->addTaskGroup($name, $description);
            if ($id === null) {
                $this->stderr(Yii::t('app', 'Can not add a task group') . PHP_EOL, Console::FG_RED);
                return 1;
            }
            $groupsIds[$name] = $id;
        }

        /*
         * Create task types
         */
        $types = [
            'payment' => Yii::t('app', 'Payment'),
            'full_payment' => Yii::t('app', 'Full Payment'),
            'additional_item_payment' => Yii::t('app', 'Additional item Payment'),
            'pay_to_author' => Yii::t('app', 'Pay to Author'),
            'other' => Yii::t('app', 'Other'),
            'call' => Yii::t('app', 'Call'),
            'email' => Yii::t('app', 'Email'),
            'vk' => Yii::t('app', 'VK'),
            'expose' => Yii::t('app', 'Expose'),
            'author' => Yii::t('app', 'Author'),
            'competence' => Yii::t('app', 'Competence'),
            'ready' => Yii::t('app', 'Ready'),
            'adjustments' => Yii::t('app', 'Adjustments'),
            'author_answer' => Yii::t('app', 'Author Answer'),
            'client_answer' => Yii::t('app', 'Client Answer'),
            'two_weeks' => Yii::t('app', 'Two Weeks'),
            'month' => Yii::t('app', 'Month'),
            'check' => Yii::t('app', 'Check'),

        ];
        $typesIds = [];
        foreach ($types as $name => $description) {
            $id = $this->addTaskType($name, $description);
            if ($id === null) {
                $this->stderr(Yii::t('app', 'Can not add a task type') . PHP_EOL, Console::FG_RED);
                return 1;
            }
            $typesIds[$name] = $id;
        }

        /*
         * Create task types to task groups relationships
         */
        $relationships = [
            'finance' => [
                'payment', 'full_payment', 'additional_item_payment',
                'pay_to_author', 'other',
            ],
            'communication' => [
                'call', 'email', 'vk'
            ],
            'urgent' => [
                'call', 'expose', 'payment',
            ],
            'work' => [
                'author', 'competence', 'ready',
                'adjustments', 'author_answer', 'client_answer',
            ],
            'check' => [
                    'two_weeks', 'month', 'check',
            ],
            'problem' => [
                'ready', 'adjustments', 'client_answer',
                'author_answer',
            ],
        ];
        foreach ($relationships as $group => $types) {

            if (!isset($groupsIds[$group])) {
                $this->stderr(
                    Yii::t('app', 'Group not found') . ': ' . $group . PHP_EOL,
                    Console::FG_RED
                );
                return 1;
            }

            foreach ($types as $type) {

                if (!isset($typesIds[$type])) {
                    $this->stderr(
                        Yii::t('app', 'Type not found') . ': ' . $type . PHP_EOL,
                        Console::FG_RED
                    );
                    return 1;
                }

                $taskTypeToTaskGroup = new TaskTypeToTaskGroup();
                $taskTypeToTaskGroup->task_group_id = $groupsIds[$group];
                $taskTypeToTaskGroup->task_type_id = $typesIds[$type];
                $taskTypeToTaskGroup->intersection_name = $group . '_' . $type;
                if (!$taskTypeToTaskGroup->save()) {
                    $this->stderr(
                        Yii::t('app', 'Can not save task group') . PHP_EOL,
                        Console::FG_RED
                    );
                    return 1;
                }

            }
        }

        $this->stdout(Yii::t('app', 'Done') . '.' . PHP_EOL, Console::FG_GREEN);

        return 0;
    }

    /**
     * @param string $name
     * @param string $description
     * @return null|string
     */
    protected function addTaskGroup($name, $description)
    {
        $taskGroup = new TaskGroup();
        $taskGroup->name = $name;
        $taskGroup->description = $description;
        if (!$taskGroup->save()) {
            return null;
        }

        return $taskGroup->id;
    }

    /**
     * @param string $name
     * @param string $description
     * @return null|string
     */
    protected function addTaskType($name, $description)
    {
        $taskType = new TaskType();
        $taskType->name = $name;
        $taskType->description = $description;
        if (!$taskType->save()) {
            return null;
        }

        return $taskType->id;
    }

    /**
     * @param int $taskGroupId
     * @param int $taskTypeId
     * @return bool
     */
    protected function addTaskTypeToTaskGroup($taskGroupId, $taskTypeId)
    {
        $taskTypeToTaskGroup = new TaskTypeToTaskGroup();
        $taskTypeToTaskGroup->task_group_id = $taskGroupId;
        $taskTypeToTaskGroup->task_type_id = $taskTypeId;
        return $taskTypeToTaskGroup->save();
    }

}
