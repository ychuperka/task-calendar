<?php

namespace app\components;

use Yii;
use yii\base\Behavior;
use yii\base\ErrorException;
use yii\db\ActiveRecord;
use yii\web\ForbiddenHttpException;

class RoleOwner extends Behavior
{

    private $role;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'afterSave',
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->role === null) {
            return true;
        }

        $role = Yii::$app->authManager->getRole($this->role);
        if ($role === null) {
            $this->owner->addError('role', Yii::t('app', 'Role not found'));
            return false;
        }
    }

    public function afterFind()
    {
        if ($this->owner->id === null) {
            return;
        }

        $roles = Yii::$app->authManager->getRolesByUser($this->owner->id);
        if (count($roles) == 0) {
            return;
        }

        $this->role = array_keys($roles)[0];
    }

    public function afterSave()
    {
        if ($this->role === null) {
            return;
        }

        $uid = $this->owner->id;
        $am = Yii::$app->authManager;
        $am->revokeAll($uid);

        $role = $am->getRole($this->role);
        $am->assign($role, $uid);
    }

    public function beforeDelete()
    {
        if (Yii::$app->user->id == $this->owner->id) {
            throw new ForbiddenHttpException(Yii::t('app', 'You can not delete yourself'));
        }

        Yii::$app->authManager->revokeAll($this->owner->id);
    }

    public function setRole($value)
    {
        $this->role = $value;
    }

    public function getRole()
    {
        return $this->role;
    }
}