<?php

namespace app\components;

use Yii;
use yii\base\Behavior;
use yii\base\UnknownPropertyException;
use yii\db\ActiveRecord;

class PasswordOwner extends Behavior
{
    public $hash_property;
    private $password;
    private $check;

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($value)
    {
        $this->password = $value;
    }

    public function getCheck()
    {
        return $this->check;
    }

    public function setCheck($value)
    {
        $this->check = $value;
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    }

    public function beforeValidate()
    {
        if ($this->password === null && $this->check === null) {
            return false;
        }

        if ($this->password !== $this->check) {
            $error = Yii::t('app', 'Passwords not match');
            $this->owner->addError('password', $error);
            $this->owner->addError('check', $error);
            return false;
        }

        return true;
    }

    public function beforeSave()
    {
        if (!$this->password) {
            return;
        }
        $this->owner->{$this->hash_property} = Yii::$app->security->generatePasswordHash($this->password);
    }
}